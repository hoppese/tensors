(* ::Package:: *)

Paclet[
  Name -> "Tensors",
  Version -> "0.1.0",
  MathematicaVersion -> "8+",
  Creator -> "Seth Hopper",
  Description -> "An application for performing tensor calculations.",
  Extensions -> {
    { "Kernel",
	  "Context" -> {"Tensors`"}
	},

    {"Documentation",
     Language -> "English", 
     MainPage -> "Guides/Tensors",
     Resources -> 
     	{"Guides/Tensors"}
    }
  }
]
