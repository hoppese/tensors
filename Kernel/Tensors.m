(* ::Package:: *)

(* Mathematica Package *)

BeginPackage["Tensors`"]
(* Exported symbols added here with SymbolName::usage *) 

Begin["`Private`"]
(* Implementation of the package *)



packages={"Tensors`TensorDefinitions`",
			"Tensors`CommonTensors`",
			"Tensors`TensorDerivatives`",
			"Tensors`TensorManipulation`"};


End[]

EndPackage[]

