Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 
   0}},ExpressionUUID->"6171d40c-45e9-4115-95db-a2ad68fcb072"],

Cell[TextData[{
 ButtonBox["Tensors",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["Tensors`TensorDefinitions",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors`TensorDefinitions"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["ActOnTensorValues",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/ref/ActOnTensorValues"]
}], "LinkTrail",ExpressionUUID->"76fe924f-79f4-47f1-afcd-ef3a4d9757bb"],

Cell[CellGroupData[{

Cell["ActOnTensorValues", \
"ObjectName",ExpressionUUID->"e318e76d-15e0-4dff-a83e-4ab962196e1f"],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       ButtonBox["ActOnTensorValues",
        BaseStyle->"Link",
        ButtonData->"paclet:Tensors/ref/ActOnTensorValues"], "[", 
       StyleBox[
        RowBox[{"f", ",", "t"}], "TI"], "]"}]], "InlineFormula",
      ExpressionUUID->"f4f9b3a8-e416-4f0e-9eeb-71915e4c30b7"],
     "\[LineSeparator]acts with the functions f on the values of Tensor t and \
returns the resulting tensor."
    }],ExpressionUUID->"7805488d-3227-4e81-87c1-d4ad8d700caa"]}
  }]], "Usage",ExpressionUUID->"26717d95-1bb6-4386-96a3-68d20351a5b8"]
}, Open  ]],

Cell[CellGroupData[{

Cell[" ", "FooterCell",ExpressionUUID->"1ef53961-fe61-4297-a0df-366810a0090c"],

Cell[BoxData[""],ExpressionUUID->"16c4df78-6bb4-4e9c-a9fb-096f9da4d088"]
}, Open  ]]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"ActOnTensorValues",
Visible->True,
PrivateNotebookOptions->{"FileOutlineCache"->False},
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Tensors`", 
    "keywords" -> {
     "ActOnTensorValues", "ACTONTENSORVALUES", "actontensorvalues"}, "index" -> 
    True, "label" -> "Tensors/Tensors`TensorDefinitions Symbol", "language" -> 
    "en", "paclet" -> "Tensors`TensorDefinitions", "status" -> "None", 
    "summary" -> 
    "ActOnTensorValues[f,t] acts with the functions f on the values of Tensor \
t and returns the resulting tensor.", 
    "synonyms" -> {
     "ActOnTensorValues", "ACTONTENSORVALUES", "actontensorvalues"}, "title" -> 
    "ActOnTensorValues", "windowTitle" -> "ActOnTensorValues", "type" -> 
    "Symbol", "uri" -> "Tensors/ref/ActOnTensorValues", "WorkflowDockedCell" -> 
    ""}, "SearchTextTranslated" -> "", "LinkTrails" -> ""},
TrackCellChangeTimes->False,
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2018)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]

