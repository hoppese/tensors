Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 
   0}},ExpressionUUID->"01e25d17-daa5-4154-85b6-b6d7b1a12e70"],

Cell[TextData[{
 ButtonBox["Tensors",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["Tensors`TensorManipulation",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors`TensorManipulation"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["MergeTensors",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/ref/MergeTensors"]
}], "LinkTrail",ExpressionUUID->"b1c3bc72-f00c-4642-a549-f4e35f72cd26"],

Cell[BoxData[GridBox[{
   {Cell["TENSORS`TENSORMANIPULATION PACLET SYMBOL", "PacletNameCell",
     ExpressionUUID->"6eae66ea-bfef-492d-bb68-3f9c76dba50e"], Cell[TextData[{
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"See Also \[RightGuillemet]\"\>",
        StripOnInput->False], {"ContractIndices":>Documentation`HelpLookup[
       "paclet:Tensors/ref/ContractIndices"], "MultiplyTensors":>
       Documentation`HelpLookup["paclet:Tensors/ref/MultiplyTensors"], 
       "MultiplyTensorScalar":>Documentation`HelpLookup[
       "paclet:Tensors/ref/MultiplyTensorScalar"], "SumTensors":>
       Documentation`HelpLookup["paclet:Tensors/ref/SumTensors"]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0},ExpressionUUID->
      "a420421e-b14b-43ab-97b4-4bbc38ea0b49"],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]"
    }], "AnchorBar",ExpressionUUID->"587027d1-bc46-4a20-84b4-e0e0342527d9"]}
  }]], "AnchorBarGrid",ExpressionUUID->"5d83cbbe-83a7-4593-8f42-d35a48538cbe"],

Cell[CellGroupData[{

Cell["MergeTensors", \
"ObjectName",ExpressionUUID->"64db971d-c239-4653-8136-384f9f949e1f"],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       ButtonBox["MergeTensors",
        BaseStyle->"Link",
        ButtonData->"paclet:Tensors/ref/MergeTensors"], "[", 
       StyleBox[
        RowBox[{"expr", ",", "n"}], "TI"], "]"}]], "InlineFormula",
      ExpressionUUID->"b11c953d-a94c-4578-bec6-d95ef874918b"],
     "\[LineSeparator]calls MultiplyTensors, MultiplyTensorScalar, \
SumTensors, and ContractIndices to merge the Tensor expression expr into one \
Tensor with TensorName n."
    }],ExpressionUUID->"840ea58d-93f5-43f2-88d0-b83ee70276c7"]},
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       ButtonBox["MergeTensors",
        BaseStyle->"Link",
        ButtonData->"paclet:Tensors/ref/MergeTensors"], "[", 
       StyleBox["expr", "TI"], "]"}]], "InlineFormula",ExpressionUUID->
      "40088c4d-047d-4b72-a480-812edcc2d880"],
     "\[LineSeparator]merges the Tensor expression expr and forms a new \
TensorName and TensorDisplayName from a combination of the Tensors making up \
the expression."
    }],ExpressionUUID->"b57878f1-9cd8-48e3-a754-8320adb986d6"]}
  }]], "Usage",ExpressionUUID->"a82f734c-65cc-42a5-981d-db179fd91ff9"]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       Cell[
        BoxData[
         ButtonBox[
          FrameBox[
           StyleBox[
            RowBox[{"MORE", " ", "INFORMATION"}], "NotesFrameText"], 
           StripOnInput -> False], Appearance -> {Automatic, None}, BaseStyle -> 
          None, ButtonFunction :> (FrontEndExecute[{
             FrontEnd`SelectionMove[
              FrontEnd`SelectedNotebook[], All, ButtonCell], 
             FrontEndToken["OpenCloseGroup"], 
             FrontEnd`SelectionMove[
              FrontEnd`SelectedNotebook[], After, CellContents]}]& ), 
          Evaluator -> None, Method -> "Preemptive"]]]], "NotesSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "Details and Options"}], 
        "NotesSection"], Appearance -> {Automatic, None}, BaseStyle -> None, 
       ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& ), Evaluator -> 
       None, Method -> "Preemptive"]]], None}, {None, None}},
 CellFrameLabelMargins->
  0,ExpressionUUID->"bdc1b375-37c6-4f97-8c03-57ff0788fecb"],

Cell["\<\
It is generally simpler to call MergeTensors than to individually call the \
other functions that it calls.\
\>", "Notes",ExpressionUUID->"3c72847d-ef9b-4c0d-ba31-1bc4f8ac8e9c"],

Cell["\<\
For complicated expressions, MergeTensors may initially fail to completely \
merge the expression into one Tensor.  By default MergeTensors will call \
itself again and continue to combine terms. The number of times it may call \
itself  is controlled by the Option NestQuantity.\
\>", "Notes",ExpressionUUID->"5f549dac-7040-4882-b62b-e3163bb19d27"],

Cell["The following options can be given: ", \
"Notes",ExpressionUUID->"79c3c326-8858-4d7b-b033-96696a3fa908"],

Cell[BoxData[GridBox[{
   {Cell["      ", "TableRowIcon",ExpressionUUID->
     "4129f60f-5015-4e34-99b0-64f62de87920"], "ActWith", "Identity", 
    Cell["\<\
Function that is applied to the elements of the tensor as they are calculated.\
\>", "TableText",ExpressionUUID->"660cb23e-11ab-44d9-b32a-3654b4ca1bb1"]},
   {Cell["      ", "TableRowIcon",ExpressionUUID->
     "c2505fac-e848-4a12-ae26-7b5a603aae26"], "ActWithNested", "Identity", 
    Cell["\<\
Function that is applied to the elements of the tensor and also passed to any \
other functions called internally.\
\>", "TableText",ExpressionUUID->"f7fa0a24-61cd-418a-9c4c-3d9975ecb453"]},
   {Cell["      ", "TableRowIcon",ExpressionUUID->
     "97109107-4e83-456f-94e7-9b169a022967"], "NestQuantity", "3", Cell["\<\
Number of times MergeTensors can call itself as it continues to try to create \
one Tensor from an expression\
\>", "TableText",ExpressionUUID->"bb09c419-0502-4866-a962-5ed3848c8324"]}
  },
  GridBoxAlignment->{
   "Columns" -> {Left, Left, {Left}}, "ColumnsIndexed" -> {}, 
    "Rows" -> {{Baseline}}, "RowsIndexed" -> {}}]], "3ColumnTableMod",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, 
   "Rows" -> {None, None, {None}}, "RowsIndexed" -> {}},
 GridBoxDividers->{
  "Rows" -> {{
     True, True, True, 
      True}}}},ExpressionUUID->"0d122c3b-6120-4412-b3f0-ca795e2a94a9"]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "PrimaryExamplesSection",
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       ButtonBox[
       "EXAMPLES", BaseStyle -> None, Appearance -> {Automatic, None}, 
        Evaluator -> None, Method -> "Preemptive", 
        ButtonFunction :> (FrontEndExecute[{
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], All, ButtonCell], 
           FrontEndToken["OpenCloseGroup"], 
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], After, CellContents]}]& )]], 
      "PrimaryExamplesSection", CellTags -> "PrimaryExamplesSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "Examples"}], 
        "PrimaryExamplesSection", CellTags -> "PrimaryExamplesSection"], 
       BaseStyle -> None, Appearance -> {Automatic, None}, Evaluator -> None, 
       Method -> "Preemptive", ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& )]]], None}, {
   None, None}},
 CellFrameLabelMargins->0,
 CellTags->
  "PrimaryExamplesSection",ExpressionUUID->"e4f4aff6-e1a3-4210-b5be-\
9540014440ab"],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(6)", "ExampleCount",ExpressionUUID->
  "f467baff-b7a1-4266-885a-fbee420192fd"]
}], "ExampleSection",ExpressionUUID->"eaf25819-1813-4cb2-a628-ad1bbc4e99f7"],

Cell[CellGroupData[{

Cell["gRN = ToMetric[\"ReissnerNordstrom\"]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"3cd832a0-6920-41f7-a786-8177d8613119"],

Cell[BoxData[
 TagBox[
  SubsuperscriptBox["g", "\[Alpha]\[Beta]", ""],
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"1aa035f2-9a9c-4b68-bd09-6a613cfb6606"]
}, Open  ]],

Cell[CellGroupData[{

Cell["ricTRN = RicciTensor[gRN]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"5b1bca4d-897e-451d-992a-3d236fd387ef"],

Cell[BoxData[
 TagBox[
  SubsuperscriptBox["R", "\[Beta]\[Gamma]", ""],
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"6b4c2934-c100-487e-aa18-bdef38ee3cdd"]
}, Open  ]],

Cell[CellGroupData[{

Cell["ricSRN = RicciScalar[gRN]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"5752425f-62da-4486-a965-8dd8bff1d6a6"],

Cell[BoxData[
 TagBox["\<\"R\"\>",
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"4be7e0db-a3c8-4f71-b100-294494d1459b"]
}, Open  ]],

Cell[CellGroupData[{

Cell["einRNExpr = ricTRN[-\[Alpha], -\[Beta]] - gRN[-\[Alpha], -\[Beta]] \
ricSRN/2", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"a3a7a2b2-bae5-4cd2-a317-c65899efe2d0"],

Cell[BoxData[
 RowBox[{
  TagBox[
   SubsuperscriptBox["R", "\[Alpha]\[Beta]", ""],
   DisplayForm], "-", 
  FractionBox[
   RowBox[{
    TagBox["\<\"R\"\>",
     DisplayForm], " ", 
    TagBox[
     SubsuperscriptBox["g", "\[Alpha]\[Beta]", ""],
     DisplayForm]}], "2"]}]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"bf5c0869-628b-4e0d-b33b-ee7897763eb7"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
einRN = MergeTensors[einRNExpr, {\"EinsteinRN\", \"G\"}, ActWith -> Simplify]\
\
\>", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"71134220-a839-4e50-b59c-5efb852a6cf7"],

Cell[BoxData[
 TagBox[
  SubsuperscriptBox["G", "\[Alpha]\[Beta]", ""],
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"c56119bc-cc52-4f63-a271-4ec95924885f"]
}, Open  ]],

Cell[CellGroupData[{

Cell["einRN // TensorValues", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"5026a6e5-3509-4696-a62b-7e537272221c"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     FractionBox[
      RowBox[{
       SuperscriptBox["Q", "2"], " ", 
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox["Q", "2"], "+", 
         RowBox[{"r", " ", 
          RowBox[{"(", 
           RowBox[{
            RowBox[{
             RowBox[{"-", "2"}], " ", "M"}], "+", "r"}], ")"}]}]}], ")"}]}], 
      SuperscriptBox["r", "6"]], ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", 
     RowBox[{"-", 
      FractionBox[
       SuperscriptBox["Q", "2"], 
       RowBox[{
        SuperscriptBox["r", "2"], " ", 
        RowBox[{"(", 
         RowBox[{
          SuperscriptBox["Q", "2"], "+", 
          RowBox[{"r", " ", 
           RowBox[{"(", 
            RowBox[{
             RowBox[{
              RowBox[{"-", "2"}], " ", "M"}], "+", "r"}], ")"}]}]}], 
         ")"}]}]]}], ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", 
     FractionBox[
      SuperscriptBox["Q", "2"], 
      SuperscriptBox["r", "2"]], ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", 
     FractionBox[
      RowBox[{
       SuperscriptBox["Q", "2"], " ", 
       SuperscriptBox[
        RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}], 
      SuperscriptBox["r", "2"]]}], "}"}]}], "}"}]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"00eeceb8-fe68-49eb-b889-8ba42add4d85"]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "SeeAlsoSection",
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       ButtonBox[
       "SEE ALSO", BaseStyle -> None, Appearance -> {Automatic, None}, 
        Evaluator -> None, Method -> "Preemptive", 
        ButtonFunction :> (FrontEndExecute[{
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], All, ButtonCell], 
           FrontEndToken["OpenCloseGroup"], 
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], After, CellContents]}]& )]], 
      "SeeAlsoSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "See Also"}], "SeeAlsoSection"], 
       BaseStyle -> None, Appearance -> {Automatic, None}, Evaluator -> None, 
       Method -> "Preemptive", ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& )]]], None}, {
   None, None}},
 CellFrameLabelMargins->
  0,ExpressionUUID->"8c8444ea-dbda-4c3c-81da-14383383bf0c"],

Cell[TextData[{
 Cell[BoxData[
  StyleBox[
   ButtonBox["ContractIndices",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/ContractIndices"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "62d3edf5-8125-4299-9148-6a39aa8bea38"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["MultiplyTensors",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/MultiplyTensors"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "78f36ad1-164a-4e50-91f9-51a77e4faea4"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["MultiplyTensorScalar",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/MultiplyTensorScalar"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "f07ea8f9-1775-4ae5-8e95-ed50cd9109f2"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["SumTensors",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/SumTensors"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "85c09b98-d01c-4c8f-83be-9439bc47e1cb"]
}], "SeeAlso",ExpressionUUID->"a2bc167a-7f7d-4fb1-9590-dc96df159f64"]
}, Open  ]],

Cell[CellGroupData[{

Cell[" ", "FooterCell",ExpressionUUID->"7d41a08f-f5fc-47b4-848b-939e3b64853a"],

Cell[BoxData[""],ExpressionUUID->"7c161f02-4fca-4e56-9e7a-b65f9dff72a1"]
}, Open  ]]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"MergeTensors",
Visible->True,
PrivateNotebookOptions->{"FileOutlineCache"->False},
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Tensors`", 
    "keywords" -> {"MergeTensors", "MERGETENSORS", "mergetensors"}, "index" -> 
    True, "label" -> "Tensors/Tensors`TensorManipulation Symbol", "language" -> 
    "en", "paclet" -> "Tensors`TensorManipulation", "status" -> "None", 
    "summary" -> 
    "MergeTensors[expr,n] calls MultiplyTensors, MultiplyTensorScalar, \
SumTensors, and ContractIndices to merge the Tensor expression expr into one \
Tensor with TensorName n.\nMergeTensors[expr] merges the Tensor expression \
expr and forms a new TensorName and TensorDisplayName from a combination of \
the Tensors making up the expression.", 
    "synonyms" -> {"MergeTensors", "MERGETENSORS", "mergetensors"}, "title" -> 
    "MergeTensors", "windowTitle" -> "MergeTensors", "type" -> "Symbol", 
    "uri" -> "Tensors/ref/MergeTensors", "WorkflowDockedCell" -> ""}, 
  "SearchTextTranslated" -> "", "LinkTrails" -> "", "NewStyles" -> False},
TrackCellChangeTimes->False,
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2018)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]

