Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 
   0}},ExpressionUUID->"b53e555d-9852-417b-8328-f366747bcd2b"],

Cell[TextData[{
 ButtonBox["Tensors",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["Tensors`CommonTensors",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors`CommonTensors"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["CottonTensor",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/ref/CottonTensor"]
}], "LinkTrail",ExpressionUUID->"aa96d0f7-dd47-40d9-8df3-131c92a545cf"],

Cell[BoxData[GridBox[{
   {Cell["TENSORS`COMMONTENSORS PACLET SYMBOL", "PacletNameCell",
     ExpressionUUID->"aabacc34-13cf-4a03-a082-17c6d6587ceb"], Cell[TextData[{
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"See Also \[RightGuillemet]\"\>",
        StripOnInput->False], {"ChristoffelSymbol":>Documentation`HelpLookup[
       "paclet:Tensors/ref/ChristoffelSymbol"], "RiemannTensor":>
       Documentation`HelpLookup["paclet:Tensors/ref/RiemannTensor"], 
       "RicciTensor":>Documentation`HelpLookup[
       "paclet:Tensors/ref/RicciTensor"], "RicciScalar":>
       Documentation`HelpLookup["paclet:Tensors/ref/RicciScalar"], 
       "EinsteinTensor":>Documentation`HelpLookup[
       "paclet:Tensors/ref/EinsteinTensor"], "WeylTensor":>
       Documentation`HelpLookup["paclet:Tensors/ref/WeylTensor"]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0},ExpressionUUID->
      "d0fabc35-ec22-49c2-b3b5-62e9d92e124b"],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]"
    }], "AnchorBar",ExpressionUUID->"8dcc7ac9-07e9-4e7b-940a-f4c377e086c3"]}
  }]], "AnchorBarGrid",ExpressionUUID->"808de6c8-11e1-4731-9726-458a998f1d57"],

Cell[CellGroupData[{

Cell["CottonTensor", \
"ObjectName",ExpressionUUID->"99b2276d-1456-44a2-b61c-e5f6426febb7"],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       ButtonBox["CottonTensor",
        BaseStyle->"Link",
        ButtonData->"paclet:Tensors/ref/CottonTensor"], "[", 
       StyleBox["m", "TI"], "]"}]], "InlineFormula",ExpressionUUID->
      "5899dead-6f5d-4d3e-87f4-1627cfacb4db"],
     "\[LineSeparator]returns the Cotton Tensor with index positions \
{\"Down\",\"Down\",\"Down\",} computed from the metric Tensor m."
    }],ExpressionUUID->"58efbf83-5e11-4b97-acb1-7627ece1e573"]}
  }]], "Usage",ExpressionUUID->"0abcbd91-86d3-4fbf-8f46-2f176e8b412f"]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       Cell[
        BoxData[
         ButtonBox[
          FrameBox[
           StyleBox[
            RowBox[{"MORE", " ", "INFORMATION"}], "NotesFrameText"], 
           StripOnInput -> False], Appearance -> {Automatic, None}, BaseStyle -> 
          None, ButtonFunction :> (FrontEndExecute[{
             FrontEnd`SelectionMove[
              FrontEnd`SelectedNotebook[], All, ButtonCell], 
             FrontEndToken["OpenCloseGroup"], 
             FrontEnd`SelectionMove[
              FrontEnd`SelectedNotebook[], After, CellContents]}]& ), 
          Evaluator -> None, Method -> "Preemptive"]]]], "NotesSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "Details and Options"}], 
        "NotesSection"], Appearance -> {Automatic, None}, BaseStyle -> None, 
       ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& ), Evaluator -> 
       None, Method -> "Preemptive"]]], None}, {None, None}},
 CellFrameLabelMargins->
  0,ExpressionUUID->"bd9dd30b-f0cc-4096-8ce1-bad4c1c9a26c"],

Cell["The following options can be given: ", \
"Notes",ExpressionUUID->"a0af1b9f-7e47-45c9-992c-20d1f13046fa"],

Cell[BoxData[GridBox[{
   {Cell["      ", "TableRowIcon",ExpressionUUID->
     "91505de5-6561-4a26-a32f-849c2cf53f48"], "ActWith", "Identity", 
    Cell["\<\
Function that is applied to the elements of the tensor as they are calculated.\
\>", "TableText",ExpressionUUID->"3af40bf0-b375-4aa9-bd99-b8b18b9e1a99"]},
   {Cell["      ", "TableRowIcon",ExpressionUUID->
     "fa10a041-98c9-4b2d-b0bd-96fe581c4f6b"], "ActWithNested", "Identity", 
    Cell["\<\
Function that is applied to the elements of the tensor and also passed to any \
other functions called internally.\
\>", "TableText",ExpressionUUID->"b4105471-21b1-4d88-9c66-19df48b7f358"]}
  },
  GridBoxAlignment->{
   "Columns" -> {Left, Left, {Left}}, "ColumnsIndexed" -> {}, 
    "Rows" -> {{Baseline}}, "RowsIndexed" -> {}}]], "3ColumnTableMod",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, "Rows" -> {None, {None}}, 
   "RowsIndexed" -> {}},
 GridBoxDividers->{
  "Rows" -> {{
     True, True, 
      True}}}},ExpressionUUID->"f7043da6-47fa-4d24-8272-4d3b77996bc7"]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "PrimaryExamplesSection",
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       ButtonBox[
       "EXAMPLES", BaseStyle -> None, Appearance -> {Automatic, None}, 
        Evaluator -> None, Method -> "Preemptive", 
        ButtonFunction :> (FrontEndExecute[{
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], All, ButtonCell], 
           FrontEndToken["OpenCloseGroup"], 
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], After, CellContents]}]& )]], 
      "PrimaryExamplesSection", CellTags -> "PrimaryExamplesSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "Examples"}], 
        "PrimaryExamplesSection", CellTags -> "PrimaryExamplesSection"], 
       BaseStyle -> None, Appearance -> {Automatic, None}, Evaluator -> None, 
       Method -> "Preemptive", ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& )]]], None}, {
   None, None}},
 CellFrameLabelMargins->0,
 CellTags->
  "PrimaryExamplesSection",ExpressionUUID->"029b9cbd-ba41-4621-8792-\
ad67edf0aa12"],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(3)", "ExampleCount",ExpressionUUID->
  "fb424fe2-c67b-48eb-b143-4dcd3b7a9c51"]
}], "ExampleSection",ExpressionUUID->"9f3ee258-446e-43f9-836a-a0dee4626bce"],

Cell[CellGroupData[{

Cell["gRN = ToMetric[\"ReissnerNordstrom\"]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"d442423a-6d00-4662-aab2-80d0b060b67a"],

Cell[BoxData[
 TagBox[
  SubsuperscriptBox["g", "\[Alpha]\[Beta]", ""],
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"96764d45-308c-4bad-8aef-09ae2a5e4c0a"]
}, Open  ]],

Cell[CellGroupData[{

Cell["CRN = CottonTensor[gRN, \"ActWith\" -> Simplify]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"28a77e49-bfb3-4415-8fc1-fb638320ed19"],

Cell[BoxData[
 TagBox[
  SubsuperscriptBox["C", "\[Alpha]\[Beta]\[Gamma]", ""],
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"93bb01eb-b1ef-48d4-96eb-182e304ff79d"]
}, Open  ]],

Cell[CellGroupData[{

Cell["TensorValues[CRN]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"1cc53e5a-b9f7-49a9-898f-34fc648fdee6"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", 
       RowBox[{"-", 
        FractionBox[
         RowBox[{"4", " ", 
          SuperscriptBox["Q", "2"], " ", 
          RowBox[{"(", 
           RowBox[{
            SuperscriptBox["Q", "2"], "+", 
            RowBox[{"r", " ", 
             RowBox[{"(", 
              RowBox[{
               RowBox[{
                RowBox[{"-", "2"}], " ", "M"}], "+", "r"}], ")"}]}]}], 
           ")"}]}], 
         SuperscriptBox["r", "7"]]}], ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{
       FractionBox[
        RowBox[{"4", " ", 
         SuperscriptBox["Q", "2"], " ", 
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox["Q", "2"], "+", 
           RowBox[{"r", " ", 
            RowBox[{"(", 
             RowBox[{
              RowBox[{
               RowBox[{"-", "2"}], " ", "M"}], "+", "r"}], ")"}]}]}], ")"}]}], 
        SuperscriptBox["r", "7"]], ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", 
       FractionBox[
        RowBox[{"2", " ", 
         SuperscriptBox["Q", "2"]}], 
        SuperscriptBox["r", "3"]], ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", 
       RowBox[{"-", 
        FractionBox[
         RowBox[{"2", " ", 
          SuperscriptBox["Q", "2"]}], 
         SuperscriptBox["r", "3"]]}], ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", 
       FractionBox[
        RowBox[{"2", " ", 
         SuperscriptBox["Q", "2"], " ", 
         SuperscriptBox[
          RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}], 
        SuperscriptBox["r", "3"]]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", 
       RowBox[{"-", 
        FractionBox[
         RowBox[{"2", " ", 
          SuperscriptBox["Q", "2"], " ", 
          SuperscriptBox[
           RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}], 
         SuperscriptBox["r", "3"]]}], ",", "0", ",", "0"}], "}"}]}], "}"}]}], 
  "}"}]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"d46709a9-755c-4a10-b668-be1bd6260c90"]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "SeeAlsoSection",
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       ButtonBox[
       "SEE ALSO", BaseStyle -> None, Appearance -> {Automatic, None}, 
        Evaluator -> None, Method -> "Preemptive", 
        ButtonFunction :> (FrontEndExecute[{
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], All, ButtonCell], 
           FrontEndToken["OpenCloseGroup"], 
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], After, CellContents]}]& )]], 
      "SeeAlsoSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "See Also"}], "SeeAlsoSection"], 
       BaseStyle -> None, Appearance -> {Automatic, None}, Evaluator -> None, 
       Method -> "Preemptive", ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& )]]], None}, {
   None, None}},
 CellFrameLabelMargins->
  0,ExpressionUUID->"0403d88f-7fa5-4d30-b8cb-1434e44f2988"],

Cell[TextData[{
 Cell[BoxData[
  StyleBox[
   ButtonBox["ChristoffelSymbol",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/ChristoffelSymbol"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "ca3197af-05a4-40aa-8cad-e830dd34785a"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["RiemannTensor",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/RiemannTensor"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "1970c43e-74bb-40e0-9b70-9951465763b5"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["RicciTensor",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/RicciTensor"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "157f7b90-a4c4-47a5-92a3-131d3daf95ee"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["RicciScalar",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/RicciScalar"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "8e59f470-bc15-49a1-8eb4-99913ba49999"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["EinsteinTensor",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/EinsteinTensor"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "622540c7-b151-4102-8bd2-6597189dbc85"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["WeylTensor",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/WeylTensor"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "cc6fd312-ae5d-4c76-8051-f9d4f2258d23"]
}], "SeeAlso",ExpressionUUID->"da0e0baf-06c7-4783-9941-7058c0fbeb7f"]
}, Open  ]],

Cell[CellGroupData[{

Cell[" ", "FooterCell",ExpressionUUID->"745bf527-39b5-4840-8790-3f434841f574"],

Cell[BoxData[""],ExpressionUUID->"59d63fd1-b160-4534-a0a6-82c3858342f8"]
}, Open  ]]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"CottonTensor",
Visible->True,
PrivateNotebookOptions->{"FileOutlineCache"->False},
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Tensors`", 
    "keywords" -> {"CottonTensor", "COTTONTENSOR", "cottontensor"}, "index" -> 
    True, "label" -> "Tensors/Tensors`CommonTensors Symbol", "language" -> 
    "en", "paclet" -> "Tensors`CommonTensors", "status" -> "None", "summary" -> 
    "CottonTensor[m] returns the Cotton Tensor with index positions \
{\"Down\",\"Down\",\"Down\",} computed from the metric Tensor m.", 
    "synonyms" -> {"CottonTensor", "COTTONTENSOR", "cottontensor"}, "title" -> 
    "CottonTensor", "windowTitle" -> "CottonTensor", "type" -> "Symbol", 
    "uri" -> "Tensors/ref/CottonTensor", "WorkflowDockedCell" -> ""}, 
  "SearchTextTranslated" -> "", "LinkTrails" -> "", "NewStyles" -> False},
TrackCellChangeTimes->False,
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2018)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]

