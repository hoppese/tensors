Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 
   0}},ExpressionUUID->"7052391c-440f-4640-aafe-dd7f2f0b12ca"],

Cell[TextData[{
 ButtonBox["Tensors",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["Tensors`TensorDefinitions",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors`TensorDefinitions"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["SetRawTensorValues",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/ref/SetRawTensorValues"]
}], "LinkTrail",ExpressionUUID->"97582ad4-c85f-407c-b4a0-4aa033bb471e"],

Cell[CellGroupData[{

Cell["SetRawTensorValues", \
"ObjectName",ExpressionUUID->"126a349a-83b1-4ab2-b672-bc0fa846cd46"],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       ButtonBox["SetRawTensorValues",
        BaseStyle->"Link",
        ButtonData->"paclet:Tensors/ref/SetRawTensorValues"], "[", 
       StyleBox[
        RowBox[{"t", ",", "vals"}], "TI"], "]"}]], "InlineFormula",
      ExpressionUUID->"17e7691f-41f4-4e0c-80ed-38f9a875dcbb"],
     "\[LineSeparator]returns the Tensor t with its RawTensorValues set to \
vals."
    }],ExpressionUUID->"0564617a-af17-410b-bcdc-d9393be50f5d"]}
  }]], "Usage",ExpressionUUID->"6bf0e4c6-d31e-486b-aa68-82805d9f3f7b"]
}, Open  ]],

Cell[CellGroupData[{

Cell[" ", "FooterCell",ExpressionUUID->"d3b51fd0-8b82-4a2b-9bf3-4abd606a14a6"],

Cell[BoxData[""],ExpressionUUID->"b83bab05-b09e-42fb-9577-cea0e86047ad"]
}, Open  ]]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"SetRawTensorValues",
Visible->True,
PrivateNotebookOptions->{"FileOutlineCache"->False},
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Tensors`", 
    "keywords" -> {
     "SetRawTensorValues", "SETRAWTENSORVALUES", "setrawtensorvalues"}, 
    "index" -> True, "label" -> "Tensors/Tensors`TensorDefinitions Symbol", 
    "language" -> "en", "paclet" -> "Tensors`TensorDefinitions", "status" -> 
    "None", "summary" -> 
    "SetRawTensorValues[t,vals] returns the Tensor t with its RawTensorValues \
set to vals.", 
    "synonyms" -> {
     "SetRawTensorValues", "SETRAWTENSORVALUES", "setrawtensorvalues"}, 
    "title" -> "SetRawTensorValues", "windowTitle" -> "SetRawTensorValues", 
    "type" -> "Symbol", "uri" -> "Tensors/ref/SetRawTensorValues", 
    "WorkflowDockedCell" -> ""}, "SearchTextTranslated" -> "", "LinkTrails" -> 
  ""},
TrackCellChangeTimes->False,
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2018)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]

