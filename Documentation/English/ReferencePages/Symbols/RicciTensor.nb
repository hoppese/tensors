Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 
   0}},ExpressionUUID->"203fdf16-9378-4c21-815e-74d73379bb29"],

Cell[TextData[{
 ButtonBox["Tensors",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["Tensors`CommonTensors",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors`CommonTensors"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["RicciTensor",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/ref/RicciTensor"]
}], "LinkTrail",ExpressionUUID->"29e6dd32-688c-473b-bbae-efc2363e3a20"],

Cell[BoxData[GridBox[{
   {Cell["TENSORS`COMMONTENSORS PACLET SYMBOL", "PacletNameCell",
     ExpressionUUID->"3079d64d-9df0-44a1-bc89-2d3453955406"], Cell[TextData[{
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"See Also \[RightGuillemet]\"\>",
        StripOnInput->False], {"ChristoffelSymbol":>Documentation`HelpLookup[
       "paclet:Tensors/ref/ChristoffelSymbol"], "RiemannTensor":>
       Documentation`HelpLookup["paclet:Tensors/ref/RiemannTensor"], 
       "RicciScalar":>Documentation`HelpLookup[
       "paclet:Tensors/ref/RicciScalar"], "EinsteinTensor":>
       Documentation`HelpLookup["paclet:Tensors/ref/EinsteinTensor"], 
       "WeylTensor":>Documentation`HelpLookup[
       "paclet:Tensors/ref/WeylTensor"]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0},ExpressionUUID->
      "3d61d94f-9270-48bb-8e76-9d87b14fcef3"],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]"
    }], "AnchorBar",ExpressionUUID->"80a86882-1912-4e7a-a550-8316c8b59408"]}
  }]], "AnchorBarGrid",ExpressionUUID->"c04863bb-7ae9-49ac-96eb-85db465abb9d"],

Cell[CellGroupData[{

Cell["RicciTensor", \
"ObjectName",ExpressionUUID->"2161cf81-59cb-4c96-95ab-65bd44ed0ada"],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       ButtonBox["RicciTensor",
        BaseStyle->"Link",
        ButtonData->"paclet:Tensors/ref/RicciTensor"], "[", 
       StyleBox["m", "TI"], "]"}]], "InlineFormula",ExpressionUUID->
      "2d36f0fd-d08a-40bc-94f3-dfc46a7b22e3"],
     "\[LineSeparator]returns the Ricci Tensor with index positions \
{\"Down\",\"Down\"} computed from the metric Tensor m."
    }],ExpressionUUID->"8b47e25b-a9c1-45de-8ae6-1f01e94abaaa"]}
  }]], "Usage",ExpressionUUID->"40e4e0df-c81c-4f4b-a337-775f72ca8a06"]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       Cell[
        BoxData[
         ButtonBox[
          FrameBox[
           StyleBox[
            RowBox[{"MORE", " ", "INFORMATION"}], "NotesFrameText"], 
           StripOnInput -> False], Appearance -> {Automatic, None}, BaseStyle -> 
          None, ButtonFunction :> (FrontEndExecute[{
             FrontEnd`SelectionMove[
              FrontEnd`SelectedNotebook[], All, ButtonCell], 
             FrontEndToken["OpenCloseGroup"], 
             FrontEnd`SelectionMove[
              FrontEnd`SelectedNotebook[], After, CellContents]}]& ), 
          Evaluator -> None, Method -> "Preemptive"]]]], "NotesSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "Details and Options"}], 
        "NotesSection"], Appearance -> {Automatic, None}, BaseStyle -> None, 
       ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& ), Evaluator -> 
       None, Method -> "Preemptive"]]], None}, {None, None}},
 CellFrameLabelMargins->
  0,ExpressionUUID->"9ab4d95f-0102-4ed5-89b4-6abedb258bbe"],

Cell["The following options can be given: ", \
"Notes",ExpressionUUID->"dcb88e49-f234-4670-b26b-af61dd9a2ea3"],

Cell[BoxData[GridBox[{
   {Cell["      ", "TableRowIcon",ExpressionUUID->
     "82b812b3-1be4-4dce-bb7b-a73987edb3e7"], "ActWith", "Identity", 
    Cell["\<\
Function that is applied to the elements of the tensor as they are calculated.\
\>", "TableText",ExpressionUUID->"ba23e6fa-7d59-444c-8783-ba54ac9c5240"]},
   {Cell["      ", "TableRowIcon",ExpressionUUID->
     "be18707f-58e6-45e8-9a7d-02eb0a3de929"], "ActWithNested", "Identity", 
    Cell["\<\
Function that is applied to the elements of the tensor and also passed to any \
other functions called internally.\
\>", "TableText",ExpressionUUID->"a456b6bd-30e0-4d94-925c-12fd8554e6e7"]}
  },
  GridBoxAlignment->{
   "Columns" -> {Left, Left, {Left}}, "ColumnsIndexed" -> {}, 
    "Rows" -> {{Baseline}}, "RowsIndexed" -> {}}]], "3ColumnTableMod",
 GridBoxOptions->{
 GridBoxBackground->{
  "Columns" -> {{None}}, "ColumnsIndexed" -> {}, "Rows" -> {None, {None}}, 
   "RowsIndexed" -> {}},
 GridBoxDividers->{
  "Rows" -> {{
     True, True, 
      True}}}},ExpressionUUID->"8ded0a0a-f6d8-4807-ab99-055691a4c302"]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "PrimaryExamplesSection",
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       ButtonBox[
       "EXAMPLES", BaseStyle -> None, Appearance -> {Automatic, None}, 
        Evaluator -> None, Method -> "Preemptive", 
        ButtonFunction :> (FrontEndExecute[{
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], All, ButtonCell], 
           FrontEndToken["OpenCloseGroup"], 
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], After, CellContents]}]& )]], 
      "PrimaryExamplesSection", CellTags -> "PrimaryExamplesSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "Examples"}], 
        "PrimaryExamplesSection", CellTags -> "PrimaryExamplesSection"], 
       BaseStyle -> None, Appearance -> {Automatic, None}, Evaluator -> None, 
       Method -> "Preemptive", ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& )]]], None}, {
   None, None}},
 CellFrameLabelMargins->0,
 CellTags->
  "PrimaryExamplesSection",ExpressionUUID->"8eca159f-420f-4da0-94d6-\
d648e00a7778"],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(3)", "ExampleCount",ExpressionUUID->
  "f0564a76-dc6c-41f1-9d74-f83862e38594"]
}], "ExampleSection",ExpressionUUID->"418884d0-a298-475e-b561-dc1a4d0d012f"],

Cell[CellGroupData[{

Cell["gS = ToMetric[\"Schwarzschild\"]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"c2aaaab8-a76f-43e8-9a7a-7745339c1fa2"],

Cell[BoxData[
 TagBox[
  SubsuperscriptBox["g", "\[Alpha]\[Beta]", ""],
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"c1114c59-214e-481e-89c6-d6a336d2a2b4"]
}, Open  ]],

Cell[CellGroupData[{

Cell["ricS = RicciTensor[gS, \"ActWith\" -> Simplify]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"726d540c-da15-4485-89c8-6a760e884183"],

Cell[BoxData[
 TagBox[
  SubsuperscriptBox["R", "\[Beta]\[Gamma]", ""],
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"29e3d865-2695-4bbb-8596-4077f7c3fe1c"]
}, Open  ]],

Cell[CellGroupData[{

Cell["TensorValues[ricS]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"40e6eb1c-bf9e-4711-b36e-cdd529a8f48a"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", "0", ",", "0"}], "}"}]}], "}"}]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"66eff78a-fa17-4f67-aaf9-ab23c276a1b6"]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "SeeAlsoSection",
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       ButtonBox[
       "SEE ALSO", BaseStyle -> None, Appearance -> {Automatic, None}, 
        Evaluator -> None, Method -> "Preemptive", 
        ButtonFunction :> (FrontEndExecute[{
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], All, ButtonCell], 
           FrontEndToken["OpenCloseGroup"], 
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], After, CellContents]}]& )]], 
      "SeeAlsoSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "See Also"}], "SeeAlsoSection"], 
       BaseStyle -> None, Appearance -> {Automatic, None}, Evaluator -> None, 
       Method -> "Preemptive", ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& )]]], None}, {
   None, None}},
 CellFrameLabelMargins->
  0,ExpressionUUID->"b361c34e-13b3-409c-af58-1a0aaccba138"],

Cell[TextData[{
 Cell[BoxData[
  StyleBox[
   ButtonBox["ChristoffelSymbol",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/ChristoffelSymbol"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "f7059d88-5bad-4462-9bcd-46565ceac934"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["RiemannTensor",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/RiemannTensor"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "784118c1-9ef2-40e1-983b-467cc24354ec"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["RicciScalar",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/RicciScalar"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "e4cc7f99-f872-4b99-909f-d189d67e6dc8"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["EinsteinTensor",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/EinsteinTensor"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "aae438bd-ca9c-44f9-a388-572052e6efd1"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["WeylTensor",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/WeylTensor"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "7a46eb22-5daf-41f5-9b78-823caaf93754"]
}], "SeeAlso",ExpressionUUID->"49332d9c-3da9-4f59-951c-afbaa27f24af"]
}, Open  ]],

Cell[CellGroupData[{

Cell[" ", "FooterCell",ExpressionUUID->"bb3e86b3-15d3-40b2-b899-20c838bb17da"],

Cell[BoxData[""],ExpressionUUID->"c85ec45c-67cc-4149-9913-a9ce565bd507"]
}, Open  ]]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"RicciTensor",
Visible->True,
PrivateNotebookOptions->{"FileOutlineCache"->False},
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Tensors`", 
    "keywords" -> {"RicciTensor", "RICCITENSOR", "riccitensor"}, "index" -> 
    True, "label" -> "Tensors/Tensors`CommonTensors Symbol", "language" -> 
    "en", "paclet" -> "Tensors`CommonTensors", "status" -> "None", "summary" -> 
    "RicciTensor[m] returns the Ricci Tensor with index positions \
{\"Down\",\"Down\"} computed from the metric Tensor m.", 
    "synonyms" -> {"RicciTensor", "RICCITENSOR", "riccitensor"}, "title" -> 
    "RicciTensor", "windowTitle" -> "RicciTensor", "type" -> "Symbol", "uri" -> 
    "Tensors/ref/RicciTensor", "WorkflowDockedCell" -> ""}, 
  "SearchTextTranslated" -> "", "LinkTrails" -> "", "NewStyles" -> False},
TrackCellChangeTimes->False,
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2018)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]

