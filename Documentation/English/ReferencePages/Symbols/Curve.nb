Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 
   0}},ExpressionUUID->"8ad677f3-4662-4105-9bd2-1fb5ff37a135"],

Cell[TextData[{
 ButtonBox["Tensors",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["Tensors`TensorDefinitions",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors`TensorDefinitions"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["Curve",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/ref/Curve"]
}], "LinkTrail",ExpressionUUID->"495ce72c-c6ab-4357-80d5-e7ac9ebb7972"],

Cell[BoxData[GridBox[{
   {Cell["TENSORS`TENSORDEFINITIONS PACLET SYMBOL", "PacletNameCell",
     ExpressionUUID->"38a03181-3caf-4117-a2b4-6e1363896651"], Cell[TextData[{
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"See Also \[RightGuillemet]\"\>",
        StripOnInput->False], {"CurveQ":>Documentation`HelpLookup[
       "paclet:Tensors/ref/CurveQ"], "ToCurve":>Documentation`HelpLookup[
       "paclet:Tensors/ref/ToCurve"], "ToTensorOnCurve":>
       Documentation`HelpLookup["paclet:Tensors/ref/ToTensorOnCurve"]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0},ExpressionUUID->
      "04df0bc5-af0a-4717-8872-e3111c78abd0"],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]"
    }], "AnchorBar",ExpressionUUID->"bb8c68fe-4718-4cb8-851d-52055719e09d"]}
  }]], "AnchorBarGrid",ExpressionUUID->"99382588-5906-4087-91a8-c711ff3c4d2e"],

Cell[CellGroupData[{

Cell["Curve", \
"ObjectName",ExpressionUUID->"0105fad2-4025-4a32-b969-c19fd069c278"],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       ButtonBox["Curve",
        BaseStyle->"Link",
        ButtonData->"paclet:Tensors/ref/Curve"], "[", 
       StyleBox["t", "TI"], "]"}]], "InlineFormula",ExpressionUUID->
      "5eb73363-c4bb-4686-94bf-9a3a4aa0b62d"],
     "\[LineSeparator]returns the curve that t is defined along."
    }],ExpressionUUID->"005caf1b-fa4f-41f7-bc6c-729586dd2de3"]}
  }]], "Usage",ExpressionUUID->"6d112ba2-0398-4125-aee5-314bc30630d0"]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       Cell[
        BoxData[
         ButtonBox[
          FrameBox[
           StyleBox[
            RowBox[{"MORE", " ", "INFORMATION"}], "NotesFrameText"], 
           StripOnInput -> False], Appearance -> {Automatic, None}, BaseStyle -> 
          None, ButtonFunction :> (FrontEndExecute[{
             FrontEnd`SelectionMove[
              FrontEnd`SelectedNotebook[], All, ButtonCell], 
             FrontEndToken["OpenCloseGroup"], 
             FrontEnd`SelectionMove[
              FrontEnd`SelectedNotebook[], After, CellContents]}]& ), 
          Evaluator -> None, Method -> "Preemptive"]]]], "NotesSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "Details and Options"}], 
        "NotesSection"], Appearance -> {Automatic, None}, BaseStyle -> None, 
       ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& ), Evaluator -> 
       None, Method -> "Preemptive"]]], None}, {None, None}},
 CellFrameLabelMargins->
  0,ExpressionUUID->"fa02f0f6-3c3b-4f9d-b964-c826c5bcf0c5"],

Cell["Curve[t] returns Undefined if t is not on a curve.", \
"Notes",ExpressionUUID->"849eb4d7-55d6-440f-9a69-c3f03b7bb27f"],

Cell["Curve[t] will return t itself if it is a curve.", \
"Notes",ExpressionUUID->"fb753bc4-4cbd-4f40-98aa-a4e9e1d08aaf"]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "PrimaryExamplesSection",
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       ButtonBox[
       "EXAMPLES", BaseStyle -> None, Appearance -> {Automatic, None}, 
        Evaluator -> None, Method -> "Preemptive", 
        ButtonFunction :> (FrontEndExecute[{
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], All, ButtonCell], 
           FrontEndToken["OpenCloseGroup"], 
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], After, CellContents]}]& )]], 
      "PrimaryExamplesSection", CellTags -> "PrimaryExamplesSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "Examples"}], 
        "PrimaryExamplesSection", CellTags -> "PrimaryExamplesSection"], 
       BaseStyle -> None, Appearance -> {Automatic, None}, Evaluator -> None, 
       Method -> "Preemptive", ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& )]]], None}, {
   None, None}},
 CellFrameLabelMargins->0,
 CellTags->
  "PrimaryExamplesSection",ExpressionUUID->"5bca026e-3453-4220-b7de-\
ed1c8f5c8201"],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(6)", "ExampleCount",ExpressionUUID->
  "1be74466-f4ef-44f2-ac86-36eaf2872641"]
}], "ExampleSection",ExpressionUUID->"92a92400-4875-4ebb-82bc-38dd4fdb3e21"],

Cell[CellGroupData[{

Cell["gK = ToMetric[\"Kerr\"]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"8e493c28-da18-4cbe-8495-c7156618440c"],

Cell[BoxData[
 TagBox[
  SubsuperscriptBox["g", "\[Alpha]\[Beta]", ""],
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"1fc49dc3-9918-4335-a116-ef56f19d36a7"]
}, Open  ]],

Cell[CellGroupData[{

Cell["\<\
c1 = ToCurve[\"x1\", gK, {t[\[Chi]], (p M)/(1 + e Cos[\[Chi]]), \[Pi]/2, \
\[Phi][\[Chi]]}, \[Chi]]\
\>", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"e4f4cb25-f216-47dd-97e0-ebfcad79ec88"],

Cell[BoxData[
 TagBox[
  RowBox[{
   SubsuperscriptBox["x1", "", "\[Alpha]"], "[", "\<\"\[Chi]\"\>", "]"}],
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"0c9db880-eaf3-4789-b2df-2147fddead30"]
}, Open  ]],

Cell[CellGroupData[{

Cell["gKC = ToTensorOnCurve[gK, c1]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"1394d2ed-c66a-4797-b997-e3e1215929a3"],

Cell[BoxData[
 TagBox[
  RowBox[{
   SubsuperscriptBox["g", "\[Alpha]\[Beta]", ""], "[", "\<\"\[Chi]\"\>", "]"}],
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"33147e80-5511-421d-b4c4-9295a0318730"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Curve[gK]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"59c5fe76-9e5d-4490-98b4-027b11e6c455"],

Cell[BoxData["Undefined"], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"82a3cbf7-86c5-4289-b51d-ac23973e0c4e"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Curve[c1]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"33889ac2-dddb-4f54-8ad9-b84aa5092b2f"],

Cell[BoxData[
 TagBox[
  RowBox[{
   SubsuperscriptBox["x1", "", "\[Alpha]"], "[", "\<\"\[Chi]\"\>", "]"}],
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"1f28da5f-25a2-4ffd-bd23-b34109c6375d"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Curve[gKC]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"2d100131-e65a-42f1-a9c6-628287b43dec"],

Cell[BoxData[
 TagBox[
  RowBox[{
   SubsuperscriptBox["x1", "", "\[Alpha]"], "[", "\<\"\[Chi]\"\>", "]"}],
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"a476c5a3-4e2b-49d7-978b-9082793ebe6e"]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "SeeAlsoSection",
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       ButtonBox[
       "SEE ALSO", BaseStyle -> None, Appearance -> {Automatic, None}, 
        Evaluator -> None, Method -> "Preemptive", 
        ButtonFunction :> (FrontEndExecute[{
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], All, ButtonCell], 
           FrontEndToken["OpenCloseGroup"], 
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], After, CellContents]}]& )]], 
      "SeeAlsoSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "See Also"}], "SeeAlsoSection"], 
       BaseStyle -> None, Appearance -> {Automatic, None}, Evaluator -> None, 
       Method -> "Preemptive", ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& )]]], None}, {
   None, None}},
 CellFrameLabelMargins->
  0,ExpressionUUID->"95e9dfe0-70ff-42d3-85b9-5ab3e39b6f12"],

Cell[TextData[{
 Cell[BoxData[
  StyleBox[
   ButtonBox["CurveQ",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/CurveQ"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "e93f7827-65c4-4c58-bf71-102b4e9eada1"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["ToCurve",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/ToCurve"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "02fc3b32-3207-48b0-99da-590be9a0344b"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["ToTensorOnCurve",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/ToTensorOnCurve"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "ca9273a7-2483-4835-8f20-b72f8794a22b"]
}], "SeeAlso",ExpressionUUID->"b84408e0-64b8-4b49-85dc-d6b297c82cf9"]
}, Open  ]],

Cell[CellGroupData[{

Cell[" ", "FooterCell",ExpressionUUID->"90ff56d7-b194-4f7b-9199-a50065058640"],

Cell[BoxData[""],ExpressionUUID->"62eb2012-bbd5-4924-8864-87142e2ab5a4"]
}, Open  ]]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"Curve",
Visible->True,
PrivateNotebookOptions->{"FileOutlineCache"->False},
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Tensors`", "keywords" -> {"Curve", "CURVE", "curve"}, 
    "index" -> True, "label" -> "Tensors/Tensors`TensorDefinitions Symbol", 
    "language" -> "en", "paclet" -> "Tensors`TensorDefinitions", "status" -> 
    "None", "summary" -> 
    "Curve[t] returns the curve that t is defined along.", 
    "synonyms" -> {"Curve", "CURVE", "curve"}, "title" -> "Curve", 
    "windowTitle" -> "Curve", "type" -> "Symbol", "uri" -> 
    "Tensors/ref/Curve", "WorkflowDockedCell" -> ""}, "SearchTextTranslated" -> 
  "", "LinkTrails" -> "", "NewStyles" -> False},
TrackCellChangeTimes->False,
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2018)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]

