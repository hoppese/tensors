Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 
   0}},ExpressionUUID->"22035e86-52be-4db9-abde-7f39beb8b47c"],

Cell[TextData[{
 ButtonBox["Tensors",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["Tensors`TensorDefinitions",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors`TensorDefinitions"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["AbstractQ",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/ref/AbstractQ"]
}], "LinkTrail",ExpressionUUID->"6f31e913-a97d-4843-bc4b-a30500942c93"],

Cell[CellGroupData[{

Cell["AbstractQ", \
"ObjectName",ExpressionUUID->"1d171af8-0810-4654-a925-3e3c156f84ed"],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       ButtonBox["AbstractQ",
        BaseStyle->"Link",
        ButtonData->"paclet:Tensors/ref/AbstractQ"], "[", 
       StyleBox["t", "TI"], "]"}]], "InlineFormula",ExpressionUUID->
      "b9e33229-268a-44ff-8439-7b974c49935a"],
     "\[LineSeparator]returns True if the Tensor t is treated as Abstract."
    }],ExpressionUUID->"63080479-27d6-4b6d-a307-730a142c9d35"]}
  }]], "Usage",ExpressionUUID->"f323b26a-d46c-4d3c-beec-019bf8cde64a"]
}, Open  ]],

Cell[CellGroupData[{

Cell[" ", "FooterCell",ExpressionUUID->"193e7362-68c3-4e1a-883e-9c22eb96e0ba"],

Cell[BoxData[""],ExpressionUUID->"7d027f82-d403-4f8d-9f2e-7346950d0bc9"]
}, Open  ]]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"AbstractQ",
Visible->True,
PrivateNotebookOptions->{"FileOutlineCache"->False},
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Tensors`", 
    "keywords" -> {"AbstractQ", "ABSTRACTQ", "abstractq"}, "index" -> True, 
    "label" -> "Tensors/Tensors`TensorDefinitions Symbol", "language" -> "en",
     "paclet" -> "Tensors`TensorDefinitions", "status" -> "None", "summary" -> 
    "AbstractQ[t] returns True if the Tensor t is treated as Abstract.", 
    "synonyms" -> {"AbstractQ", "ABSTRACTQ", "abstractq"}, "title" -> 
    "AbstractQ", "windowTitle" -> "AbstractQ", "type" -> "Symbol", "uri" -> 
    "Tensors/ref/AbstractQ", "WorkflowDockedCell" -> ""}, 
  "SearchTextTranslated" -> "", "LinkTrails" -> ""},
TrackCellChangeTimes->False,
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2018)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]

