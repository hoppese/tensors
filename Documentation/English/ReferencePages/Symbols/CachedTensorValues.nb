Notebook[{
Cell[" ", "SymbolColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 
   0}},ExpressionUUID->"cdada8fb-8c9f-41ac-91b0-df176d969d55"],

Cell[TextData[{
 ButtonBox["Tensors",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["Tensors`TensorDefinitions",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors`TensorDefinitions"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["CachedTensorValues",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/ref/CachedTensorValues"]
}], "LinkTrail",ExpressionUUID->"46db5b2f-880b-4e53-a6f4-8191b29d32ee"],

Cell[BoxData[GridBox[{
   {Cell["TENSORS`TENSORDEFINITIONS PACLET SYMBOL", "PacletNameCell",
     ExpressionUUID->"f496cdfb-8415-45a4-b46d-0cec053217b2"], Cell[TextData[{
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"See Also \[RightGuillemet]\"\>",
        StripOnInput->False], {"TensorValues":>Documentation`HelpLookup[
       "paclet:Tensors/ref/TensorValues"], "RawTensorValues":>
       Documentation`HelpLookup["paclet:Tensors/ref/RawTensorValues"], 
       "$CacheTensorValues":>Documentation`HelpLookup[
       "paclet:Tensors/ref/$CacheTensorValues"], "ClearCachedTensorValues":>
       Documentation`HelpLookup["paclet:Tensors/ref/ClearCachedTensorValues"]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0},ExpressionUUID->
      "9bffc636-6aba-4f32-9ca7-f1000b732fbc"],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]"
    }], "AnchorBar",ExpressionUUID->"48866940-7375-4648-86da-b15f033375a5"]}
  }]], "AnchorBarGrid",ExpressionUUID->"0238b86f-a0e3-44f1-bc32-829a7399bcc0"],

Cell[CellGroupData[{

Cell["CachedTensorValues", \
"ObjectName",ExpressionUUID->"317412fe-65dc-44fe-9964-2a2e894df341"],

Cell[BoxData[GridBox[{
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       ButtonBox["CachedTensorValues",
        BaseStyle->"Link",
        ButtonData->"paclet:Tensors/ref/CachedTensorValues"], "[", 
       StyleBox["n", "TI"], "]"}]], "InlineFormula",ExpressionUUID->
      "358b2c0d-073f-4291-81f1-02b1cb149638"],
     "\[LineSeparator]returns a List of Rules showing all cached expressions \
for the TensorName n (stored in the Symbol RawTensorValues)."
    }],ExpressionUUID->"d71f4439-1b0e-4cbb-9dc8-57bb2173af80"]},
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       ButtonBox["CachedTensorValues",
        BaseStyle->"Link",
        ButtonData->"paclet:Tensors/ref/CachedTensorValues"], "[", 
       StyleBox["t", "TI"], "]"}]], "InlineFormula",ExpressionUUID->
      "4ded2b17-5819-4989-aa26-1c59ca85f30b"],
     "\[LineSeparator]returns a List of Rules showing all cached expressions \
for the Tensor t (stored in the Symbol RawTensorValues)."
    }],ExpressionUUID->"5fa85fe6-8c4d-461a-9d99-2ad6772b3fd1"]},
   {"", Cell[TextData[{
     Cell[BoxData[
      RowBox[{
       ButtonBox["CachedTensorValues",
        BaseStyle->"Link",
        ButtonData->"paclet:Tensors/ref/CachedTensorValues"], "[", 
       StyleBox["All", "TI"], "]"}]], "InlineFormula",ExpressionUUID->
      "8aca1231-20dd-4a05-a498-ca26cba78f42"],
     "\[LineSeparator]returns a List of Rules showing all cached expressions \
(stored in the Symbol RawTensorValues)."
    }],ExpressionUUID->"4eb6c50a-b7ee-4c04-9537-1d40a8808102"]}
  }]], "Usage",ExpressionUUID->"ba713016-2261-478a-b5c4-27c50875c408"]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "NotesSection",
 WholeCellGroupOpener->True,
 CellGroupingRules->{"SectionGrouping", 50},
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       Cell[
        BoxData[
         ButtonBox[
          FrameBox[
           StyleBox[
            RowBox[{"MORE", " ", "INFORMATION"}], "NotesFrameText"], 
           StripOnInput -> False], Appearance -> {Automatic, None}, BaseStyle -> 
          None, ButtonFunction :> (FrontEndExecute[{
             FrontEnd`SelectionMove[
              FrontEnd`SelectedNotebook[], All, ButtonCell], 
             FrontEndToken["OpenCloseGroup"], 
             FrontEnd`SelectionMove[
              FrontEnd`SelectedNotebook[], After, CellContents]}]& ), 
          Evaluator -> None, Method -> "Preemptive"]]]], "NotesSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "Details and Options"}], 
        "NotesSection"], Appearance -> {Automatic, None}, BaseStyle -> None, 
       ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& ), Evaluator -> 
       None, Method -> "Preemptive"]]], None}, {None, None}},
 CellFrameLabelMargins->
  0,ExpressionUUID->"010394a1-3909-481b-863d-6f302374c87b"],

Cell["\<\
By default RawTensorValues are not cached. To do so, set $CacheTensorValues = \
True.\
\>", "Notes",ExpressionUUID->"779116f8-ddb7-46ad-8ba8-905f9c320463"],

Cell["\<\
CachedTensorValues are stored as Values in the Symbol RawTensorValues using \
TensorName and IndexPositions as Keys.\
\>", "Notes",ExpressionUUID->"fe3ee23c-94e4-4c1a-aa11-29689a3a1070"],

Cell["TensorNames ending in \"-Auto\" are not cached.", \
"Notes",ExpressionUUID->"7809357c-80b3-4ebb-9a24-1a3ffad20092"]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "PrimaryExamplesSection",
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       ButtonBox[
       "EXAMPLES", BaseStyle -> None, Appearance -> {Automatic, None}, 
        Evaluator -> None, Method -> "Preemptive", 
        ButtonFunction :> (FrontEndExecute[{
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], All, ButtonCell], 
           FrontEndToken["OpenCloseGroup"], 
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], After, CellContents]}]& )]], 
      "PrimaryExamplesSection", CellTags -> "PrimaryExamplesSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "Examples"}], 
        "PrimaryExamplesSection", CellTags -> "PrimaryExamplesSection"], 
       BaseStyle -> None, Appearance -> {Automatic, None}, Evaluator -> None, 
       Method -> "Preemptive", ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& )]]], None}, {
   None, None}},
 CellFrameLabelMargins->0,
 CellTags->
  "PrimaryExamplesSection",ExpressionUUID->"e82813bb-c874-4f14-b426-\
da6b05ac5a14"],

Cell[CellGroupData[{

Cell[TextData[{
 "Basic Examples",
 "\[NonBreakingSpace]\[NonBreakingSpace]",
 Cell["(8)", "ExampleCount",ExpressionUUID->
  "f8a1e75a-b62c-44ca-b2bd-b1688c9763df"]
}], "ExampleSection",ExpressionUUID->"37ddcdec-b6e0-4b4a-a173-06b591dfce0b"],

Cell[CellGroupData[{

Cell["$CacheTensorValues = True", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"f28c8245-8c3f-4390-988e-d957259999e2"],

Cell[BoxData["True"], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"675e8495-7a0a-4122-9856-2516f77caa8d"]
}, Open  ]],

Cell[CellGroupData[{

Cell["gK = ToMetric[\"Kerr\"]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"b986d692-d500-4bd8-ab2a-57a301f157dd"],

Cell[BoxData[
 TagBox[
  SubsuperscriptBox["g", "\[Alpha]\[Beta]", ""],
  DisplayForm]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"4b30a8a4-9524-4016-b086-310a73548e61"]
}, Open  ]],

Cell[CellGroupData[{

Cell["TensorValues[gK]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"b6a3054b-1e84-4c08-b0eb-3692571f2b6b"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{
     FractionBox[
      RowBox[{
       RowBox[{"-", 
        SuperscriptBox["a", "2"]}], "+", 
       RowBox[{"2", " ", "M", " ", "r"}], "-", 
       SuperscriptBox["r", "2"], "+", 
       RowBox[{
        SuperscriptBox["a", "2"], " ", 
        SuperscriptBox[
         RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}]}], 
      RowBox[{
       SuperscriptBox["r", "2"], "+", 
       RowBox[{
        SuperscriptBox["a", "2"], " ", 
        SuperscriptBox[
         RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}]], ",", "0", ",", "0",
      ",", 
     RowBox[{"-", 
      FractionBox[
       RowBox[{"2", " ", "a", " ", "M", " ", "r", " ", 
        SuperscriptBox[
         RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}], 
       RowBox[{
        SuperscriptBox["r", "2"], "+", 
        RowBox[{
         SuperscriptBox["a", "2"], " ", 
         SuperscriptBox[
          RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}]]}]}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", 
     FractionBox[
      RowBox[{
       SuperscriptBox["r", "2"], "+", 
       RowBox[{
        SuperscriptBox["a", "2"], " ", 
        SuperscriptBox[
         RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}], 
      RowBox[{
       SuperscriptBox["a", "2"], "-", 
       RowBox[{"2", " ", "M", " ", "r"}], "+", 
       SuperscriptBox["r", "2"]}]], ",", "0", ",", "0"}], "}"}], ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "0", ",", 
     RowBox[{
      SuperscriptBox["r", "2"], "+", 
      RowBox[{
       SuperscriptBox["a", "2"], " ", 
       SuperscriptBox[
        RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}], ",", "0"}], "}"}], 
   ",", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"-", 
      FractionBox[
       RowBox[{"2", " ", "a", " ", "M", " ", "r", " ", 
        SuperscriptBox[
         RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}], 
       RowBox[{
        SuperscriptBox["r", "2"], "+", 
        RowBox[{
         SuperscriptBox["a", "2"], " ", 
         SuperscriptBox[
          RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}]]}], ",", "0", ",", 
     "0", ",", 
     FractionBox[
      RowBox[{
       SuperscriptBox[
        RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"], " ", 
       RowBox[{"(", 
        RowBox[{
         SuperscriptBox[
          RowBox[{"(", 
           RowBox[{
            SuperscriptBox["a", "2"], "+", 
            SuperscriptBox["r", "2"]}], ")"}], "2"], "-", 
         RowBox[{
          SuperscriptBox["a", "2"], " ", 
          RowBox[{"(", 
           RowBox[{
            SuperscriptBox["a", "2"], "-", 
            RowBox[{"2", " ", "M", " ", "r"}], "+", 
            SuperscriptBox["r", "2"]}], ")"}], " ", 
          SuperscriptBox[
           RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}]}], ")"}]}], 
      RowBox[{
       SuperscriptBox["r", "2"], "+", 
       RowBox[{
        SuperscriptBox["a", "2"], " ", 
        SuperscriptBox[
         RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}]]}], "}"}]}], 
  "}"}]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"02697767-711f-4ca8-8ad1-517741ba6a4a"]
}, Open  ]],

Cell[CellGroupData[{

Cell["CachedTensorValues[gK]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"b3998731-67d7-46ec-91a3-9a23c50608e8"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"\<\"KerrMetric\"\>", ",", 
     RowBox[{"{", 
      RowBox[{"\<\"Down\"\>", ",", "\<\"Down\"\>"}], "}"}]}], "}"}], 
   "\[Rule]", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       FractionBox[
        RowBox[{
         RowBox[{"-", 
          SuperscriptBox["a", "2"]}], "+", 
         RowBox[{"2", " ", "M", " ", "r"}], "-", 
         SuperscriptBox["r", "2"], "+", 
         RowBox[{
          SuperscriptBox["a", "2"], " ", 
          SuperscriptBox[
           RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}]}], 
        RowBox[{
         SuperscriptBox["r", "2"], "+", 
         RowBox[{
          SuperscriptBox["a", "2"], " ", 
          SuperscriptBox[
           RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}]], ",", "0", ",", 
       "0", ",", 
       RowBox[{"-", 
        FractionBox[
         RowBox[{"2", " ", "a", " ", "M", " ", "r", " ", 
          SuperscriptBox[
           RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}], 
         RowBox[{
          SuperscriptBox["r", "2"], "+", 
          RowBox[{
           SuperscriptBox["a", "2"], " ", 
           SuperscriptBox[
            RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}]]}]}], "}"}], ",", 
     
     RowBox[{"{", 
      RowBox[{"0", ",", 
       FractionBox[
        RowBox[{
         SuperscriptBox["r", "2"], "+", 
         RowBox[{
          SuperscriptBox["a", "2"], " ", 
          SuperscriptBox[
           RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}], 
        RowBox[{
         SuperscriptBox["a", "2"], "-", 
         RowBox[{"2", " ", "M", " ", "r"}], "+", 
         SuperscriptBox["r", "2"]}]], ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", 
       RowBox[{
        SuperscriptBox["r", "2"], "+", 
        RowBox[{
         SuperscriptBox["a", "2"], " ", 
         SuperscriptBox[
          RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}], ",", "0"}], "}"}], 
     ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", 
        FractionBox[
         RowBox[{"2", " ", "a", " ", "M", " ", "r", " ", 
          SuperscriptBox[
           RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}], 
         RowBox[{
          SuperscriptBox["r", "2"], "+", 
          RowBox[{
           SuperscriptBox["a", "2"], " ", 
           SuperscriptBox[
            RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}]]}], ",", "0", ",",
        "0", ",", 
       FractionBox[
        RowBox[{
         SuperscriptBox[
          RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"], " ", 
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{
              SuperscriptBox["a", "2"], "+", 
              SuperscriptBox["r", "2"]}], ")"}], "2"], "-", 
           RowBox[{
            SuperscriptBox["a", "2"], " ", 
            RowBox[{"(", 
             RowBox[{
              SuperscriptBox["a", "2"], "-", 
              RowBox[{"2", " ", "M", " ", "r"}], "+", 
              SuperscriptBox["r", "2"]}], ")"}], " ", 
            SuperscriptBox[
             RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}]}], ")"}]}], 
        RowBox[{
         SuperscriptBox["r", "2"], "+", 
         RowBox[{
          SuperscriptBox["a", "2"], " ", 
          SuperscriptBox[
           RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}]]}], "}"}]}], 
    "}"}]}], "}"}]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"03e951e5-3e71-44b5-8038-4fad63e5cbd2"]
}, Open  ]],

Cell[CellGroupData[{

Cell["$CacheTensorValues = False", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"8226cce1-0abe-4e72-aa15-a0c5fb438e96"],

Cell[BoxData["False"], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"0fa44f3a-2cdb-4a33-9bd2-d507df6f0927"]
}, Open  ]],

Cell[CellGroupData[{

Cell["CachedTensorValues[gK]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"95e79a4a-195c-4c9c-b472-dc03b8348821"],

Cell[BoxData[
 RowBox[{"{", 
  RowBox[{
   RowBox[{"{", 
    RowBox[{"\<\"KerrMetric\"\>", ",", 
     RowBox[{"{", 
      RowBox[{"\<\"Down\"\>", ",", "\<\"Down\"\>"}], "}"}]}], "}"}], 
   "\[Rule]", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{
       FractionBox[
        RowBox[{
         RowBox[{"-", 
          SuperscriptBox["a", "2"]}], "+", 
         RowBox[{"2", " ", "M", " ", "r"}], "-", 
         SuperscriptBox["r", "2"], "+", 
         RowBox[{
          SuperscriptBox["a", "2"], " ", 
          SuperscriptBox[
           RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}]}], 
        RowBox[{
         SuperscriptBox["r", "2"], "+", 
         RowBox[{
          SuperscriptBox["a", "2"], " ", 
          SuperscriptBox[
           RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}]], ",", "0", ",", 
       "0", ",", 
       RowBox[{"-", 
        FractionBox[
         RowBox[{"2", " ", "a", " ", "M", " ", "r", " ", 
          SuperscriptBox[
           RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}], 
         RowBox[{
          SuperscriptBox["r", "2"], "+", 
          RowBox[{
           SuperscriptBox["a", "2"], " ", 
           SuperscriptBox[
            RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}]]}]}], "}"}], ",", 
     
     RowBox[{"{", 
      RowBox[{"0", ",", 
       FractionBox[
        RowBox[{
         SuperscriptBox["r", "2"], "+", 
         RowBox[{
          SuperscriptBox["a", "2"], " ", 
          SuperscriptBox[
           RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}], 
        RowBox[{
         SuperscriptBox["a", "2"], "-", 
         RowBox[{"2", " ", "M", " ", "r"}], "+", 
         SuperscriptBox["r", "2"]}]], ",", "0", ",", "0"}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"0", ",", "0", ",", 
       RowBox[{
        SuperscriptBox["r", "2"], "+", 
        RowBox[{
         SuperscriptBox["a", "2"], " ", 
         SuperscriptBox[
          RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}], ",", "0"}], "}"}], 
     ",", 
     RowBox[{"{", 
      RowBox[{
       RowBox[{"-", 
        FractionBox[
         RowBox[{"2", " ", "a", " ", "M", " ", "r", " ", 
          SuperscriptBox[
           RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}], 
         RowBox[{
          SuperscriptBox["r", "2"], "+", 
          RowBox[{
           SuperscriptBox["a", "2"], " ", 
           SuperscriptBox[
            RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}]]}], ",", "0", ",",
        "0", ",", 
       FractionBox[
        RowBox[{
         SuperscriptBox[
          RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"], " ", 
         RowBox[{"(", 
          RowBox[{
           SuperscriptBox[
            RowBox[{"(", 
             RowBox[{
              SuperscriptBox["a", "2"], "+", 
              SuperscriptBox["r", "2"]}], ")"}], "2"], "-", 
           RowBox[{
            SuperscriptBox["a", "2"], " ", 
            RowBox[{"(", 
             RowBox[{
              SuperscriptBox["a", "2"], "-", 
              RowBox[{"2", " ", "M", " ", "r"}], "+", 
              SuperscriptBox["r", "2"]}], ")"}], " ", 
            SuperscriptBox[
             RowBox[{"Sin", "[", "\[Theta]", "]"}], "2"]}]}], ")"}]}], 
        RowBox[{
         SuperscriptBox["r", "2"], "+", 
         RowBox[{
          SuperscriptBox["a", "2"], " ", 
          SuperscriptBox[
           RowBox[{"Cos", "[", "\[Theta]", "]"}], "2"]}]}]]}], "}"}]}], 
    "}"}]}], "}"}]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"35301daa-803b-497f-bfb2-328e6cabb9a9"]
}, Open  ]],

Cell[CellGroupData[{

Cell["ClearCachedTensorValues[gK]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"c3897d47-d61e-4f47-86d3-915e47a3f5c9"],

Cell[BoxData["Null"], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"8b2e850d-18f7-4a73-9327-c977fd87fd5a"]
}, Open  ]],

Cell[CellGroupData[{

Cell["CachedTensorValues[gK]", "Input",
 CellLabel->"In[1]:=",ExpressionUUID->"535a0d63-2e8e-4879-bb65-a0784504d0cd"],

Cell[BoxData[
 RowBox[{"{", "}"}]], "Output",
 CellLabel->"Out[1]:= ",ExpressionUUID->"01a805c8-7abb-4ad9-b885-a76a36789227"]
}, Open  ]]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "SeeAlsoSection",
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell[
      TextData[
       ButtonBox[
       "SEE ALSO", BaseStyle -> None, Appearance -> {Automatic, None}, 
        Evaluator -> None, Method -> "Preemptive", 
        ButtonFunction :> (FrontEndExecute[{
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], All, ButtonCell], 
           FrontEndToken["OpenCloseGroup"], 
           FrontEnd`SelectionMove[
            FrontEnd`SelectedNotebook[], After, CellContents]}]& )]], 
      "SeeAlsoSection"], 
     TextData[
      ButtonBox[
       Cell[
        TextData[{
          Cell[
           BoxData[
            TemplateBox[{24}, "Spacer1"]]], "See Also"}], "SeeAlsoSection"], 
       BaseStyle -> None, Appearance -> {Automatic, None}, Evaluator -> None, 
       Method -> "Preemptive", ButtonFunction :> (FrontEndExecute[{
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], All, ButtonCell], 
          FrontEndToken["OpenCloseGroup"], 
          FrontEnd`SelectionMove[
           FrontEnd`SelectedNotebook[], After, CellContents]}]& )]]], None}, {
   None, None}},
 CellFrameLabelMargins->
  0,ExpressionUUID->"4ea21e27-aa6d-40da-8c87-9c63e57e220d"],

Cell[TextData[{
 Cell[BoxData[
  StyleBox[
   ButtonBox["TensorValues",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/TensorValues"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "e7887065-1b87-43fe-8e4e-6d50123cdca2"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["RawTensorValues",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/RawTensorValues"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "efa9ddf1-be50-42f8-a8b0-755bbb637e4c"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["$CacheTensorValues",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/$CacheTensorValues"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "c26d895e-f32f-49c6-a9c7-2ed0bafa1754"],
 "\[NonBreakingSpace]",
 StyleBox["\[MediumSpace]\[FilledVerySmallSquare]\[MediumSpace]", 
  "InlineSeparator"],
 " ",
 Cell[BoxData[
  StyleBox[
   ButtonBox["ClearCachedTensorValues",
    BaseStyle->"Link",
    ButtonData->"paclet:Tensors/ref/ClearCachedTensorValues"],
   FontFamily->"Verdana"]], "InlineFormula",ExpressionUUID->
  "15f87dc8-55c6-4075-a46c-be131e5fd26e"]
}], "SeeAlso",ExpressionUUID->"bc51bb74-4592-4be7-a3f1-a933506c0b9b"]
}, Open  ]],

Cell[CellGroupData[{

Cell[" ", "FooterCell",ExpressionUUID->"67d76317-16f3-4bb0-96e8-b0e9b094a948"],

Cell[BoxData[""],ExpressionUUID->"aa318ebb-4d06-494c-a726-b3796176e616"]
}, Open  ]]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"CachedTensorValues",
Visible->True,
PrivateNotebookOptions->{"FileOutlineCache"->False},
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Tensors`", 
    "keywords" -> {
     "CachedTensorValues", "CACHEDTENSORVALUES", "cachedtensorvalues"}, 
    "index" -> True, "label" -> "Tensors/Tensors`TensorDefinitions Symbol", 
    "language" -> "en", "paclet" -> "Tensors`TensorDefinitions", "status" -> 
    "None", "summary" -> 
    "CachedTensorValues[n] returns a List of Rules showing all cached \
expressions for the TensorName n (stored in the Symbol RawTensorValues).\n\
CachedTensorValues[t] returns a List of Rules showing all cached expressions \
for the Tensor t (stored in the Symbol RawTensorValues).\n\
CachedTensorValues[All] returns a List of Rules showing all cached \
expressions (stored in the Symbol RawTensorValues).", 
    "synonyms" -> {
     "CachedTensorValues", "CACHEDTENSORVALUES", "cachedtensorvalues"}, 
    "title" -> "CachedTensorValues", "windowTitle" -> "CachedTensorValues", 
    "type" -> "Symbol", "uri" -> "Tensors/ref/CachedTensorValues", 
    "WorkflowDockedCell" -> ""}, "SearchTextTranslated" -> "", "LinkTrails" -> 
  "", "NewStyles" -> False},
TrackCellChangeTimes->False,
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2018)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]

