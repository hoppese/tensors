Notebook[{
Cell[" ", "GuideColorBar",
 CellMargins->{{Inherited, Inherited}, {-5, 
   0}},ExpressionUUID->"454d37c6-a8ee-4e54-8c12-058c34e7b09e"],

Cell[TextData[{
 ButtonBox["Tensors",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors"],
 StyleBox[" > ", "LinkTrailSeparator"],
 ButtonBox["Tensor package for black hole calculations",
  BaseStyle->{"Link", "LinkTrail"},
  ButtonData->"paclet:Tensors/guide/Tensors"]
}], "LinkTrail",ExpressionUUID->"46e81e3c-bc1a-4306-82b7-eff864fb46b3"],

Cell[BoxData[GridBox[{
   {Cell["TENSORS GUIDE", "PacletNameCell",ExpressionUUID->
     "f99f8375-0f91-4cbc-805d-150b7d044602"], Cell[TextData[{
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"Tutorials \[RightGuillemet]\"\>",
        StripOnInput->False], {
       RowBox[{"Introduction", " ", "to", " ", "Tensors"}]:>
       Documentation`HelpLookup[
       "paclet:Tensors/tutorial/Introduction to Tensors"], 
       RowBox[{"Introduction", " ", "to", " ", "Tensor", " ", "Curves"}]:>
       Documentation`HelpLookup[
       "paclet:Tensors/tutorial/Introduction to Tensor Curves"], 
       RowBox[{
       "Manipulating", " ", "and", " ", "differentiating", " ", "Tensors"}]:>
       Documentation`HelpLookup[
       "paclet:Tensors/tutorial/Manipulating and differentiating Tensors"], 
       RowBox[{"Built", " ", "in", " ", "common", " ", "Tensors"}]:>
       Documentation`HelpLookup[
       "paclet:Tensors/tutorial/Built in common Tensors"], 
       RowBox[{"Caching", " ", "Tensor", " ", "values"}]:>
       Documentation`HelpLookup[
       "paclet:Tensors/tutorial/Caching Tensor values"]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0},ExpressionUUID->
      "18b30c5c-ffca-4b17-b35e-f338d9ce9785"],
     "\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\[ThickSpace]\
\[ThickSpace]",
     Cell[BoxData[
      ActionMenuBox[
       FrameBox["\<\"URL \[RightGuillemet]\"\>",
        StripOnInput->False], {"\<\"Go to Tensors website\"\>":>
       FrontEndExecute[{
         NotebookLocate[{
           URL["http://bitbucket.org/hoppese/Tensors"], None}]}], 
       Delimiter, "\<\"Go to wolfram Mathematica website\"\>":>
       FrontEndExecute[{
         NotebookLocate[{
           URL[
           "http://reference.wolfram.com/mathematica/guide/Mathematica.html"],
            None}]}]},
       Appearance->None,
       MenuAppearance->Automatic]],
      LineSpacing->{1.4, 0},ExpressionUUID->
      "3402a6e8-3447-419b-93bc-4ddd63e46c60"]
    }], "AnchorBar",ExpressionUUID->"11590fde-e21e-4707-9bf2-9756e857ab08"]}
  }]], "AnchorBarGrid",ExpressionUUID->"275e0e2b-aecf-4279-ab53-ffbdcd34f32a"],

Cell[CellGroupData[{

Cell["Tensor package for black hole calculations", \
"GuideTitle",ExpressionUUID->"0f564a0d-85e7-4bd8-8291-cf53ed2c867e"],

Cell["\<\
The Tensors package provides functions for computing coordinate based for \
black hole calculations.\
\>", "GuideAbstract",ExpressionUUID->"6c9797ca-36c4-4366-a761-4c9ad176b2ba"],

Cell[CellGroupData[{

Cell["Tensor creation and common functions", \
"GuideFunctionsSubsection",ExpressionUUID->"dd9a1f4f-b98e-4a9e-bf31-\
7a3ef6ceb180"],

Cell[TextData[{
 Cell[TextData[ButtonBox["ToTensor",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/ToTensor"]], "InlineFunctionSans",
  ExpressionUUID->"5f82726e-dbd2-4fea-b2fc-8a3d5a87de37"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Form a new Tensor"
}], "GuideText",ExpressionUUID->"369dad2f-2b7f-4bd2-b089-bf0fd1e2345e"],

Cell[TextData[{
 Cell[TextData[ButtonBox["ToMetric",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/ToMetric"]], "InlineFunctionSans",
  ExpressionUUID->"09fc0a76-74f0-4429-abe2-431c35960349"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Form a new metric Tensor"
}], "GuideText",ExpressionUUID->"ec421bd8-2fa3-46a5-b2b3-73810f7a52de"],

Cell[TextData[{
 Cell[TextData[ButtonBox["ToCurve",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/ToCurve"]], "InlineFunctionSans",
  ExpressionUUID->"58aa7213-408b-465f-b72b-bda89cb9ecd1"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Form a new Curve Tensor"
}], "GuideText",ExpressionUUID->"8f980977-b1a5-4e08-8181-2b12438098ca"],

Cell[TextData[{
 Cell[TextData[ButtonBox["ToTensorOnCurve",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/ToTensorOnCurve"]], "InlineFunctionSans",
  ExpressionUUID->"d619e70b-3591-46f8-a4bb-5d71e76b3fd6"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Form a new Tensor on a Curve"
}], "GuideText",ExpressionUUID->"b183c172-1b4c-4d66-9f13-2a6b3ccf6179"],

Cell[TextData[{
 Cell[TextData[ButtonBox["RawTensorValues",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/RawTensorValues"]], "InlineFunctionSans",
  ExpressionUUID->"0e19921a-e63c-413b-89f5-9c727ccc3a8d"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Values stored internally by Tensor"
}], "GuideText",ExpressionUUID->"d50dc1e9-d053-4034-a6e8-f7b2459d267d"],

Cell[TextData[{
 Cell[TextData[ButtonBox["TensorValues",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/TensorValues"]], "InlineFunctionSans",
  ExpressionUUID->"bd078fef-8e4e-4ff8-8661-3771f3b60ff9"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Values stored internally by Tensor, potentially evaluated on a Curve"
}], "GuideText",ExpressionUUID->"ecfb9a18-ec7f-43fb-8cfb-742dbb37509e"],

Cell[TextData[{
 Cell[TextData[ButtonBox["Rank",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/Rank"]], "InlineFunctionSans",
  ExpressionUUID->"cdcd4b51-be67-4d39-99ae-f1ca8e7dddef"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Rank of Tensor"
}], "GuideText",ExpressionUUID->"3bd1be71-0292-4cce-845e-b13051345f32"],

Cell[TextData[{
 Cell[TextData[ButtonBox["Indices",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/Indices"]], "InlineFunctionSans",
  ExpressionUUID->"ae252d79-6ad9-4582-9831-ed3a20f3d545"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "List of Indices of Tensor"
}], "GuideText",ExpressionUUID->"8b03f474-8bcc-40b6-92e9-6a7814c928a8"],

Cell[TextData[{
 Cell[TextData[ButtonBox["PossibleIndices",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/PossibleIndices"]], "InlineFunctionSans",
  ExpressionUUID->"b15540ca-1e20-4650-aad5-01c240039d2e"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "List of all possible indices of Tensor"
}], "GuideText",ExpressionUUID->"51b7fbe8-b299-4f7c-ba6d-9283662204da"],

Cell[TextData[{
 Cell[TextData[ButtonBox["IndexPositions",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/IndexPositions"]], "InlineFunctionSans",
  ExpressionUUID->"3c2fa57d-cc11-4efe-b5d9-f5c183f9fdb9"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "List of elements \"Up\" and \"Down\" giving the positions of Tensor \
indices"
}], "GuideText",ExpressionUUID->"73ffb0ff-98b6-4aff-b9c9-b055173501a2"],

Cell[TextData[{
 Cell[TextData[ButtonBox["Dimensions",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/Dimensions"]], "InlineFunctionSans",
  ExpressionUUID->"4a4cb5d7-67de-4890-85d7-5cb24781571b"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Dimensions is overloaded. For Tensors it gives the number of dimensions of \
the manifold."
}], "GuideText",ExpressionUUID->"7ade67f9-b4c6-401d-933f-847855d487db"],

Cell[TextData[{
 Cell[TextData[ButtonBox["Coordinates",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/Coordinates"]], "InlineFunctionSans",
  ExpressionUUID->"f39ee171-d03b-4b09-bce8-73cb637ce03a"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "List of the coordinates of the Tensor"
}], "GuideText",ExpressionUUID->"884b09ee-7501-486c-8e47-cda4b9c97443"],

Cell[TextData[{
 Cell[TextData[ButtonBox["TensorRules",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/TensorRules"]], "InlineFunctionSans",
  ExpressionUUID->"d911c404-4b6e-4f5d-b784-02383604c2a8"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "List of Rules showing Tensor values for given components"
}], "GuideText",ExpressionUUID->"6cf71fb2-3890-4ad8-8817-d5a4b960dbba"],

Cell[TextData[{
 Cell[TextData[ButtonBox["TensorName",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/TensorName"]], "InlineFunctionSans",
  ExpressionUUID->"a3410e7b-8d69-499c-ba2a-3af2634506ab"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Internal name of Tensor"
}], "GuideText",ExpressionUUID->"ebf306d0-e255-47f6-81e4-cf5a33e4f749"],

Cell[TextData[{
 Cell[TextData[ButtonBox["TensorDisplayName",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/TensorDisplayName"]], "InlineFunctionSans",
  ExpressionUUID->"5d59c298-c548-4e92-9ea3-88b4c342417f"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Name used for display of Tensor in notebook"
}], "GuideText",ExpressionUUID->"33d9e015-68a3-4c8c-846e-e5a496ab59a0"],

Cell[TextData[{
 Cell[TextData[ButtonBox["Metric",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/Metric"]], "InlineFunctionSans",
  ExpressionUUID->"eba7275f-c866-402c-980b-6ff949151e0f"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Metric associated with the Tensor"
}], "GuideText",ExpressionUUID->"067603bc-85cd-44d8-ba58-ea6f591f590b"],

Cell[TextData[{
 Cell[TextData[ButtonBox["InverseMetric",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/InverseMetric"]], "InlineFunctionSans",
  ExpressionUUID->"79086b80-e95f-4e02-80e1-39fb9a6a258e"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Inverse metric associated with the Tensor"
}], "GuideText",ExpressionUUID->"baf4344b-599d-495b-a10d-bddf99b07fdc"],

Cell[TextData[{
 Cell[TextData[ButtonBox["Curve",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/Curve"]], "InlineFunctionSans",
  ExpressionUUID->"bd167821-e6cf-4c21-990c-f5cd59997177"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Curve associated with the Tensor"
}], "GuideText",ExpressionUUID->"d4da04f7-d5f5-4369-b3e4-9c8d4c5a9cc0"],

Cell[TextData[{
 Cell[TextData[ButtonBox["MetricQ",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/MetricQ"]], "InlineFunctionSans",
  ExpressionUUID->"ec9f522b-3ad9-47ca-b550-a25dfb0d8224"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Test whether a Tensor is a metric"
}], "GuideText",ExpressionUUID->"37742d52-4a69-4338-8817-2263ce979f15"],

Cell[TextData[{
 Cell[TextData[ButtonBox["CurveQ",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/CurveQ"]], "InlineFunctionSans",
  ExpressionUUID->"bf589d31-b91c-4bf5-8d04-13f096b20471"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Test whether a Tensor is a Curve"
}], "GuideText",ExpressionUUID->"7cfe7892-64f2-4ce6-92eb-bc998bd509eb"],

Cell[TextData[{
 Cell[TextData[ButtonBox["OnCurveQ",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/OnCurveQ"]], "InlineFunctionSans",
  ExpressionUUID->"b6a087c7-2adf-4654-9a1b-defbd39a9b21"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Test whether a Tensor is on a Curve"
}], "GuideText",ExpressionUUID->"5a4f32b8-3c91-48a2-8ce1-b3e4418938c9"],

Cell[TextData[{
 Cell[TextData[ButtonBox["ValidTensorExpressionQ",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/ValidTensorExpressionQ"]], 
  "InlineFunctionSans",ExpressionUUID->"c97656b7-1d0f-4a31-a9e5-c3d1bcffa676"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Test whether a Tensor expression is valid (in indices and metrics)"
}], "GuideText",ExpressionUUID->"f9311072-698c-438c-81ad-1398887bf784"],

Cell[TextData[{
 Cell[TextData[ButtonBox["ValidateTensorExpression",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/ValidateTensorExpression"]], 
  "InlineFunctionSans",ExpressionUUID->"2e0258a2-c5da-4af1-b75f-e7a81366d211"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Similar to ValidTensorExpressionQ, but aborts and prints error messages"
}], "GuideText",ExpressionUUID->"cf9c46ed-78f1-4f80-99cc-6ae1194061fe"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Tensor manipulation", \
"GuideFunctionsSubsection",ExpressionUUID->"b4d07179-6fdd-456d-b7f6-\
4746d0466836"],

Cell[TextData[{
 Cell[TextData[ButtonBox["ShiftIndices",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/ShiftIndices"]], "InlineFunctionSans",
  ExpressionUUID->"2dbbd86e-cf81-4e1e-a1ef-a0fabeb48244"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Raise and lower indices on a Tensor"
}], "GuideText",ExpressionUUID->"ac50729c-d4bf-4f71-9fee-df78b8b2b52b"],

Cell[TextData[{
 Cell[TextData[ButtonBox["MergeTensors",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/MergeTensors"]], "InlineFunctionSans",
  ExpressionUUID->"38b9584d-976b-4182-a14b-b86e9b543fc5"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Combine an expression into a single Tensor"
}], "GuideText",ExpressionUUID->"75e52eeb-cf50-4951-b59e-13fe8d43aa6c"],

Cell[TextData[{
 Cell[TextData[ButtonBox["ContractIndices",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/ContractIndices"]], "InlineFunctionSans",
  ExpressionUUID->"88cb38a4-ca35-47a8-8065-d7ef9c170e2b"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Trace over repeated indices"
}], "GuideText",ExpressionUUID->"1b3da889-67a9-4371-ac47-8ac16d72bb00"],

Cell[TextData[{
 Cell[TextData[ButtonBox["MultiplyTensors",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/MultiplyTensors"]], "InlineFunctionSans",
  ExpressionUUID->"a645f971-c0fd-4196-b125-c2155a3390ce"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Outer product of Tensors"
}], "GuideText",ExpressionUUID->"aecc5006-572e-4445-9bd1-65bbf2da2a26"],

Cell[TextData[{
 Cell[TextData[ButtonBox["MultiplyTensorScalar",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/MultiplyTensorScalar"]], 
  "InlineFunctionSans",ExpressionUUID->"9487c82b-5900-40df-93dc-de573d3850b7"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Product of a Tensor and a scalar"
}], "GuideText",ExpressionUUID->"57c7dc3c-dd57-44be-9b82-4f13652d11bd"],

Cell[TextData[{
 Cell[TextData[ButtonBox["SumTensors",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/SumTensors"]], "InlineFunctionSans",
  ExpressionUUID->"3b499ac3-5883-4673-84d9-14c6a174d6fc"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Sum of Tensors"
}], "GuideText",ExpressionUUID->"e5d03afb-8792-4de5-9369-5c6119c0c257"],

Cell[TextData[{
 Cell[TextData[ButtonBox["D",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/D"]], "InlineFunctionSans",ExpressionUUID->
  "b6c2772e-4434-457d-a206-9c7363abe9f5"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "D is overloaded. It takes the partial derivative of a Tensor."
}], "GuideText",ExpressionUUID->"319e8cae-0143-46a1-9c3d-36fa0a2aeef8"],

Cell[TextData[{
 Cell[TextData[ButtonBox["CovariantD",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/CovariantD"]], "InlineFunctionSans",
  ExpressionUUID->"94be8058-9cd9-4ebd-92b9-fd4525ce03d5"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Covariant derivative of a Tensor expression"
}], "GuideText",ExpressionUUID->"de6ff6cc-d208-45d0-ab48-2d765a263deb"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Common Tensors and functions", \
"GuideFunctionsSubsection",ExpressionUUID->"fe82a591-7caa-40f0-9f03-\
013a2b579507"],

Cell[TextData[{
 Cell[TextData[ButtonBox["ChristoffelSymbol",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/ChristoffelSymbol"]], "InlineFunctionSans",
  ExpressionUUID->"4f18f7eb-388a-4e3f-928a-6d9885e8f547"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Connection coefficients for a metric"
}], "GuideText",ExpressionUUID->"91a2bbc2-9322-4aa6-94f6-c9947dfacf2b"],

Cell[TextData[{
 Cell[TextData[ButtonBox["RiemannTensor",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/RiemannTensor"]], "InlineFunctionSans",
  ExpressionUUID->"04467bc7-cd6e-4991-85a6-fbb84323cb19"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Riemann tensor for a metric"
}], "GuideText",ExpressionUUID->"6370f685-9eb4-4117-8b29-712b4b2338b4"],

Cell[TextData[{
 Cell[TextData[ButtonBox["RicciTensor",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/RicciTensor"]], "InlineFunctionSans",
  ExpressionUUID->"f4182047-3539-4d47-8443-6a6f6ab9e570"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Ricci tensor for a metric"
}], "GuideText",ExpressionUUID->"ad093718-7ed3-4668-9c85-e2f01443cb28"],

Cell[TextData[{
 Cell[TextData[ButtonBox["RicciScalar",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/RicciScalar"]], "InlineFunctionSans",
  ExpressionUUID->"f913a9cf-b090-42e4-8f86-6801a7658f46"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Ricci scalar for a metric"
}], "GuideText",ExpressionUUID->"ba21ccd4-ca4c-4ecc-b323-609c8cbd1c75"],

Cell[TextData[{
 Cell[TextData[ButtonBox["EinsteinTensor",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/EinsteinTensor"]], "InlineFunctionSans",
  ExpressionUUID->"c59b8ffe-b201-4db3-a1a9-27583b9f58e6"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Einstein tensor for a metric"
}], "GuideText",ExpressionUUID->"3210cfaf-4408-4cb4-844c-2a2eac35f1bd"],

Cell[TextData[{
 Cell[TextData[ButtonBox["WeylTensor",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/WeylTensor"]], "InlineFunctionSans",
  ExpressionUUID->"4c3158cb-bfec-4a6f-9e70-b6cc665b98ac"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Weyl Tensor for a metric"
}], "GuideText",ExpressionUUID->"2ec39d0a-223f-44cf-a399-7c64c0a05ba9"],

Cell[TextData[{
 Cell[TextData[ButtonBox["CottonTensor",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/CottonTensor"]], "InlineFunctionSans",
  ExpressionUUID->"f447cf6c-093a-48ef-bdd6-40faab39f62e"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Cotton Tensor for a metric"
}], "GuideText",ExpressionUUID->"c09ca879-563d-4459-819d-6b23af21829e"],

Cell[TextData[{
 Cell[TextData[ButtonBox["KretschmannScalar",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/KretschmannScalar"]], "InlineFunctionSans",
  ExpressionUUID->"9c62019b-e09a-443d-a0a9-3b09a9496755"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Kretschmann scalar for a metric"
}], "GuideText",ExpressionUUID->"19efff23-1a2d-4ab4-99cf-ab3a3c88aaf4"],

Cell[TextData[{
 Cell[TextData[ButtonBox["MaxwellPotential",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/MaxwellPotential"]], "InlineFunctionSans",
  ExpressionUUID->"55f6b82d-3045-4456-9bd6-958b7823748d"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Electromagnetic four vector potential"
}], "GuideText",ExpressionUUID->"72ff6358-9223-4df3-96a6-32ceff27c4b6"],

Cell[TextData[{
 Cell[TextData[ButtonBox["FieldStrengthTensor",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/FieldStrengthTensor"]], 
  "InlineFunctionSans",ExpressionUUID->"66dd10eb-4f3f-4ec9-a0e3-e0e2f0eef96d"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Field strength tensor for a vector potential"
}], "GuideText",ExpressionUUID->"ebcc5a78-ef05-4f47-aa72-48b3a5351cf8"],

Cell[TextData[{
 Cell[TextData[ButtonBox["MaxwellStressEnergyTensor",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/MaxwellStressEnergyTensor"]], 
  "InlineFunctionSans",ExpressionUUID->"2d297945-8a04-43f1-828d-f68b90c54001"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Stress energy tensor for a field strength tensor"
}], "GuideText",ExpressionUUID->"5e22073a-4c7d-40f1-aade-3814e46f4dd0"],

Cell[TextData[{
 Cell[TextData[ButtonBox["FourVelocity",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/FourVelocity"]], "InlineFunctionSans",
  ExpressionUUID->"5963dcc7-609d-407b-9063-916cb66af3e2"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Four-velocity for common spacetimes"
}], "GuideText",ExpressionUUID->"ddfd0c0e-d26e-435b-bca1-aa7e2efcf24f"],

Cell[TextData[{
 Cell[TextData[ButtonBox["KinnersleyNullVector",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/KinnersleyNullVector"]], 
  "InlineFunctionSans",ExpressionUUID->"b889af61-7367-421c-805b-288229ea6f99"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Null vector common in Newman-Penrose calculations"
}], "GuideText",ExpressionUUID->"ce62a7a7-8265-483f-87b9-8634fdde5508"],

Cell[TextData[{
 Cell[TextData[ButtonBox["KinnersleyNullTetrad",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/KinnersleyNullTetrad"]], 
  "InlineFunctionSans",ExpressionUUID->"21688d11-ddd0-4e97-9de5-d0fec72c14f7"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "The four Kinnersley null vectors"
}], "GuideText",ExpressionUUID->"c7bc8309-a907-4566-9e87-d7ee5ee2f478"],

Cell[TextData[{
 Cell[TextData[ButtonBox["KinnersleyDerivative",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/KinnersleyDerivative"]], 
  "InlineFunctionSans",ExpressionUUID->"2262b2a3-0e94-43e2-906c-52846473b192"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Derivative associated with a Kinnersley null vector"
}], "GuideText",ExpressionUUID->"a0929aa2-5cc1-4752-b786-9d57e6717347"],

Cell[TextData[{
 Cell[TextData[ButtonBox["SpinCoefficient",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/SpinCoefficient"]], "InlineFunctionSans",
  ExpressionUUID->"64513fe3-c990-487a-9ac7-7069504780b8"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "One of 12 Newman-Penrose spin coefficients"
}], "GuideText",ExpressionUUID->"d7a632d5-9262-4e02-98fc-2538a385e26c"],

Cell[TextData[{
 Cell[TextData[ButtonBox["BianchiIdentities",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/BianchiIdentities"]], "InlineFunctionSans",
  ExpressionUUID->"4e9d8a41-ab7f-4d5f-8a65-e06743920084"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Bianchi identities for a metric"
}], "GuideText",ExpressionUUID->"732e3366-bda5-44ee-8cbd-def6f80702be"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Caching Tensor values", \
"GuideFunctionsSubsection",ExpressionUUID->"03add0cd-bf48-469e-976d-\
5e4492f9d1af"],

Cell[TextData[{
 Cell[TextData[ButtonBox["$CacheTensorValues",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/$CacheTensorValues"]], "InlineFunctionSans",
  ExpressionUUID->"c49a80ce-82c0-4179-b9c0-519d2e4e4f56"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Global variable for caching"
}], "GuideText",ExpressionUUID->"d76ef6b3-9835-4c83-a9b6-a55761e50ec3"],

Cell[TextData[{
 Cell[TextData[ButtonBox["CachedTensorValues",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/CachedTensorValues"]], "InlineFunctionSans",
  ExpressionUUID->"a3eb75db-98dd-46b6-9183-0ed9347f2414"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Values that are cached internally"
}], "GuideText",ExpressionUUID->"9cdd242a-9564-4236-99be-61d725a2b70b"],

Cell[TextData[{
 Cell[TextData[ButtonBox["ClearCachedTensorValues",
  BaseStyle->"Link",
  ButtonData->"paclet:Tensors/ref/ClearCachedTensorValues"]], 
  "InlineFunctionSans",ExpressionUUID->"923d7c45-df38-46e4-892b-8edb81268cd1"],
 " ",
 StyleBox["\[LongDash]", "GuideEmDash"],
 " ",
 "Remove internally cached values"
}], "GuideText",ExpressionUUID->"39b9bc0d-be97-49ac-b571-e45c857451d2"]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["", "GuideTutorialsSection",
 CellFrameLabels->{{
    FEPrivate`If[
     FEPrivate`Or[
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "6.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "7.0"], 
      FEPrivate`SameQ[FEPrivate`$ProductVersion, "8.0"]], 
     Cell["TUTORIALS", "GuideTutorialsSection"], 
     Cell["Tutorials", "GuideTutorialsSection"]], None}, {None, None}},
 CellFrameLabelMargins->
  0,ExpressionUUID->"abc4c015-93bd-411f-9119-6d5171d6f5da"],

Cell[TextData[ButtonBox["Introduction to Tensors",
 BaseStyle->"Link",
 ButtonData->
  "paclet:Tensors/tutorial/Introduction to Tensors"]], \
"GuideTutorial",ExpressionUUID->"1835a174-73b1-4d85-836f-2dd33a22e7e4"],

Cell[TextData[ButtonBox["Introduction to Tensor Curves",
 BaseStyle->"Link",
 ButtonData->
  "paclet:Tensors/tutorial/Introduction to Tensor Curves"]], \
"GuideTutorial",ExpressionUUID->"f2419623-78d3-4bee-9d19-0723e2c2782a"],

Cell[TextData[ButtonBox["Manipulating and differentiating Tensors",
 BaseStyle->"Link",
 ButtonData->
  "paclet:Tensors/tutorial/Manipulating and differentiating Tensors"]], \
"GuideTutorial",ExpressionUUID->"23ea7c85-e610-4c5c-85ba-eec2020b19ce"],

Cell[TextData[ButtonBox["Built in common Tensors",
 BaseStyle->"Link",
 ButtonData->
  "paclet:Tensors/tutorial/Built in common Tensors"]], \
"GuideTutorial",ExpressionUUID->"f5686c90-4b0d-4a4d-a389-d2200f912521"],

Cell[TextData[ButtonBox["Caching Tensor values",
 BaseStyle->"Link",
 ButtonData->
  "paclet:Tensors/tutorial/Caching Tensor values"]], \
"GuideTutorial",ExpressionUUID->"311a9af7-4914-4e1f-8b74-e98cc47d21f6"]
}, Open  ]],

Cell[CellGroupData[{

Cell[" ", "FooterCell",ExpressionUUID->"abe92339-f77f-47b9-b34f-2eed17ce9bbb"],

Cell[BoxData[""],ExpressionUUID->"47f4621f-0e67-46e4-abef-ef3a24b112ab"]
}, Open  ]]
},
Saveable->False,
ScreenStyleEnvironment->"Working",
WindowSize->{725, 750},
WindowMargins->{{0, Automatic}, {Automatic, 0}},
WindowTitle->"Tensor package for black hole calculations",
Visible->True,
PrivateNotebookOptions->{"FileOutlineCache"->False},
TaggingRules->{
 "ModificationHighlight" -> False, 
  "Metadata" -> {
   "context" -> "Tensors`", "keywords" -> {"Tensors"}, "index" -> True, 
    "label" -> "Tensors Application", "language" -> "en", "paclet" -> 
    "Tensors", "status" -> "None", "summary" -> 
    "Tensor package providing functions for black hole calculations.", 
    "synonyms" -> {"Tensors"}, "title" -> 
    "Tensor package for black hole calculations", "windowTitle" -> 
    "Tensor package for black hole calculations", "type" -> "Guide", "uri" -> 
    "Tensors/guide/Tensors", "WorkflowDockedCell" -> ""}, 
  "SearchTextTranslated" -> "", "LinkTrails" -> ""},
TrackCellChangeTimes->False,
FrontEndVersion->"11.3 for Mac OS X x86 (32-bit, 64-bit Kernel) (March 5, \
2018)",
StyleDefinitions->FrontEnd`FileName[{"Wolfram"}, "Reference.nb", 
  CharacterEncoding -> "UTF-8"]
]

