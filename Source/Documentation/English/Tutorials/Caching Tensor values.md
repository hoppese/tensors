{
 "Application" -> "Tensors",
 "Title" -> "Caching Tensor values",
 "Related Tutorials" -> {"Introduction to Tensors","Introduction to Tensor Curves","Manipulating and Differentiating Tensors","Built in common Tensors"},
 "Keywords" -> {"Tensors", "Cache"},
 "Label" -> "Tensor caching",
 "Package" -> "Caching Tensor values",
 "Summary" -> "Optimizing Tensor calculations through caching.",
 "Synonyms" -> {}
 }
