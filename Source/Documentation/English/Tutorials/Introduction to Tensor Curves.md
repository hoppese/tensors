{
 "Application" -> "Tensors",
 "Title" -> "Introduction to Tensor Curves",
 "Related Tutorials" -> {"Introduction to Tensors","Caching Tensor values","Built in common Tensors","Manipulating and Differentiating Tensors"},
 "Keywords" -> {"Tensors", "Curves"},
 "Label" -> "Tensors curves intro",
 "Package" -> "Introduction to Tensor Curves",
 "Summary" -> "Forming Tensor Curves and performing common actions.",
 "Synonyms" -> {}
 }
