{
 "Application" -> "Tensors",
 "Title" -> "Manipulating and Differentiating Tensors",
 "Related Tutorials" -> {"Introduction to Tensors","Introduction to Tensor Curves","Caching Tensor values","Built in common Tensors"},
 "Keywords" -> {"Tensors"},
 "Label" -> "Manipulating intro",
 "Package" -> "Manipulating and Differentiating Tensors",
 "Summary" -> "Introduction to taking derivatives of tensors and performing general manipulation.",
 "Synonyms" -> {}
 }
