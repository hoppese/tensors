{
 "Application" -> "Tensors",
 "Title" -> "Introduction to Tensors",
 "Related Tutorials" -> {"Introduction to Tensor Curves","Caching Tensor values","Manipulating and Differentiating Tensors","Built in common Tensors"},
 "Keywords" -> {"Tensors"},
 "Label" -> "Tensors intro",
 "Package" -> "Introduction to Tensors",
 "Summary" -> "Forming Tensors and performing common actions",
 "Synonyms" -> {}
 }
