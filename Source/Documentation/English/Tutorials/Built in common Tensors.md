{
 "Application" -> "Tensors",
 "Title" -> "Common built in Tensors",
 "Related Tutorials" -> {"Introduction to Tensors","Introduction to Tensor Curves","Caching Tensor values","Manipulating and Differentiating Tensors"},
 "Keywords" -> {"Tensors"},
 "Label" -> "Tensors intro",
 "Package" -> "Built in common Tensors",
 "Summary" -> "Built in common Tensors",
 "Synonyms" -> {}
 }
