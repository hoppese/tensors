{
  "More Information" -> {
  },

  "Basic Examples" -> {
      "uS = FourVelocity[\"SchwarzschildGeneric\"]",
      "xS = Curve[uS]",
      "CurveRules[xS]"
    },

    "See Also" ->
    {"Curve","TensorRules"}

}
