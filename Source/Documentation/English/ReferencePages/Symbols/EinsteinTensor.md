{
  "More Information" -> {
  },

  "Basic Examples" -> {
    "gS = ToMetric[\"Schwarzschild\"]",
    "einS = EinsteinTensor[gS, \"ActWith\" -> Simplify]",
    "TensorValues[einS]"
    },

    "See Also" ->
    {"ChristoffelSymbol","RiemannTensor","RicciTensor","RicciScalar","WeylTensor"}

}
