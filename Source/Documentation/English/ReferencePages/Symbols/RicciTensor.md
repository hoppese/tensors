{
  "More Information" -> {
  },

  "Basic Examples" -> {
    "gS = ToMetric[\"Schwarzschild\"]",
    "ricS = RicciTensor[gS, \"ActWith\" -> Simplify]",
    "TensorValues[ricS]"
    },

    "See Also" ->
    {"ChristoffelSymbol","RiemannTensor","RicciScalar","EinsteinTensor","WeylTensor"}

}
