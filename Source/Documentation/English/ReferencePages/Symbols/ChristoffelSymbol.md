{
  "More Information" -> {
  },

  "Basic Examples" -> {
    "gS = ToMetric[\"Schwarzschild\"]",
    "chrS = ChristoffelSymbol[gS, \"ActWith\" -> Simplify]",
    "chrS[t,-t,-r]"
    },

    "See Also" ->
    {"RiemannTensor","RicciTensor","RicciScalar","EinsteinTensor","WeylTensor"}

}
