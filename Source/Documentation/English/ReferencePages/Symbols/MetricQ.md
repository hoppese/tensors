{
  "More Information" -> {
  },

  "Basic Examples" -> {
      "gK = ToMetric[\"Kerr\"]",
      "MetricQ[gK]",
      "MetricQ[ChristoffelSymbol[gK]]"
    },

    "See Also" ->
    {"Metric","ToMetric"}

}
