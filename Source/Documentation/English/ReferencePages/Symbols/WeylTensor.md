{
  "More Information" -> {
  },

  "Basic Examples" -> {
    "gRN = ToMetric[\"ReissnerNordstrom\"]",
    "CRN = WeylTensor[gRN, \"ActWith\" -> Simplify]",
    "TensorValues[CRN]"
    },

    "See Also" ->
    {"ChristoffelSymbol","RiemannTensor","RicciTensor","RicciScalar","EinsteinTensor","CottonTensor"}

}
