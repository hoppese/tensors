{
  "More Information" -> {

  },

  "Basic Examples" -> {
    "gK = ToMetric[\"Kerr\"]",
    "c1 = ToCurve[\"x1\", gK, {t[\[Chi]], (p M)/(1 + e Cos[\[Chi]]), \[Pi]/2, \[Phi][\[Chi]]}, \[Chi]]"
    },

    "See Also" ->
    {"ToMetric","ToTensor","TensorValues","ToTensorOnCurve","Curve","CurveQ","ParametrizedValuesQ"}

}
