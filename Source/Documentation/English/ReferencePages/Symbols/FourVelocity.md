{
  "More Information" -> {
  },

  "Basic Examples" -> {
      "uS = FourVelocity[\"SchwarzschildGeneric\"]",
      "TensorValues[uS]",
      "OnCurveQ[uS]",
      "uSq = MergeTensors[uS[\[Alpha]]uS[-\[Alpha]],ActWith->Simplify]",
      "TensorValues[uSq]"
    },

    "See Also" ->
    {"Curve","OnCurveQ","ToCurve","ToTensorOnCurve","ToTensorFieldOnCurve"}

}
