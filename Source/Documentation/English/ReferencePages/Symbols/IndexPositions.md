{
  "More Information" -> {
  },

  "Basic Examples" -> {
    "gS = ToMetric[\"Schwarzschild\"]",
    "IndexPositions[gS]",
    "rieS = RiemannTensor[gS]",
    "IndexPositions[rieS]"
    },

    "See Also" ->
    {"PossibleIndices","Indices","Rank","Coordinates"}

}
